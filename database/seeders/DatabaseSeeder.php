<?php

namespace Database\Seeders;

use App\Models\ContractInvoiceStatus;
use App\Models\Order;
use App\Models\Client;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(){

        $u = Client::find(10);
        $u->password = password_hash('1234567', PASSWORD_BCRYPT, [
            'cost' => 10,
        ]);
        if(empty($u->name)){
            $u->name = "Иван Петров";
        }
        $u->save();

        $this->contractInvoiceStatuses();
        $order = Order::find(1);
        $order->date_added = now();
        $order->date_panning_start = now()->addDay();
        $order->save();
    }

    private function contractInvoiceStatuses(){
        ContractInvoiceStatus::updateOrInsert(['status_id' => ContractInvoiceStatus::ID_NOT_PAID], ['status_name' => 'Не оплачен']);
        ContractInvoiceStatus::updateOrInsert(['status_id' => ContractInvoiceStatus::ID_PAID],['status_name' => 'Оплачен']);
    }
}
