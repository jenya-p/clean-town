<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Contract;
use App\Models\ContractInvoice;
use App\Models\News;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\OrderStatus;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Staff;
use App\Models\StaffFunction;
use App\Models\StaffStatus;
use App\Models\TruckType;
use App\Models\Client;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class NewsAndNotificationsSeeder extends Seeder {

    public static function clear() {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('news')->truncate();
        DB::table('notifications')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }


    public function run() {

        self::clear();

        srand(0);


        for ($i = 1; $i < 25; $i++) {
            $this->news();
        }

        for ($i = 1; $i < 25; $i++) {
            $this->notification();
        }


    }


    public function news() {
        $f = Factory::create('ru_RU');

        $news = News::create([
            'title' => $f->sentence(5),
            'text' => $f->realText(200),
            'url' => $f->url,
            'media_path' => null,
            'media_name' => null,
        ]);

        $path = 'news-media/' . $news->id;
        $path = Storage::putFile($path, resource_path('test_imgs/' . random_int(1, 2) . '.jpg'));

        $news->media_path = $path;
        $news->media_name = 'test-file.jpeg';
        $news->save();
    }

    public function notification() {
        $f = Factory::create('ru_RU');

        Notification::create([
            'title' => $f->sentence(5),
            'text' => $f->realText(200),
        ]);

    }


}
