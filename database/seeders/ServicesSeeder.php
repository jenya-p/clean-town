<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\AnnexTemplate;
use App\Models\AnnexTemplateService;
use App\Models\Contract;
use App\Models\ContractInvoice;
use App\Models\News;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\OrderStatus;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Staff;
use App\Models\StaffFunction;
use App\Models\StaffStatus;
use App\Models\TruckType;
use App\Models\Client;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ServicesSeeder extends Seeder {


    public static function clear() {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('services_to_service_types')->truncate();
        DB::table('user_services')->truncate();
        DB::table('services')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        self::clear();

        srand(0);

        $this->services();

        $serviceIds = Service::pluck('service_id')->toArray();
        $userIds = Client::pluck('user_id')->toArray();
        $serviceTypeIds = ServiceType::pluck('service_type_id')->toArray();


        foreach ($serviceIds as $sId) {
            $stIds = Arr::random($serviceTypeIds, rand(2, count($serviceTypeIds)));
            foreach ($stIds as $stId) {


                DB::table('services_to_service_types')->insertOrIgnore([
                    'service_id' => $sId,
                    'service_type_id' => $stId
                ]);
            }
        }

        $templateIds = AnnexTemplate::pluck('id')->toArray();
        $createAnnex = function($sId) use($templateIds) {
            $useDriverPayment = rand(0, 1);
            return AnnexTemplateService::firstOrCreate([
                'service_id' => $sId,
                'annex_template_id' => Arr::random($templateIds),
                'contract_price' => rand(10,1000) * 100.00,
                'use_driver_payment' => $useDriverPayment,
                'driver_payment' => $useDriverPayment ? rand(10,500) * 100.00 : 0,
                'sort' => 0
            ]);
        };


        $contractsIds = Contract::pluck('contract_id')->toArray();
        foreach ($serviceIds as $sId) {
            foreach ($contractsIds as $cId) {
                if (rand(0,4) == 0) continue;

                $annex = $createAnnex($sId);

                DB::table('contract_annex_template')->insertOrIgnore([
                    'contract_id' => $cId,
                    'annex_template_id' => $annex->id,
                    'date_start' => date('Y-m-d', time())
                ]);
            }
        }

        $objectIds = Contract::pluck('contract_id')->toArray();
        foreach ($serviceIds as $sId) {
            foreach ($objectIds as $oId) {
                if (rand(0,4) == 0) continue;

                $annex = $createAnnex($sId);

                DB::table('object_annex_template')->insertOrIgnore([
                    'object_id' => $oId,
                    'annex_template_id' => $annex->id,
                    'date_start' => date('Y-m-d', time())
                ]);
            }
        }



//        foreach ($serviceIds as $sId) {
//            foreach ($userIds as $uId) {
//                if (rand(0,7) == 0) continue;
//                DB::table('user_services')->insertOrIgnore([
//                    'service_id' => $sId,
//                    'user_id' => $uId
//                ]);
//            }
//        }


    }



    public function services() {
        $data = [
            ['Вывоз ТКО IV класс опасности 0,8м³', 950],
            ['Вывоз ТКО IV класс опасности 1,1м³', 1250],
            ['Вывоз ТКО IV класс опасности 8м³', 9500],
            ['Вывоз ТКО IV класс опасности 10м³', 11500],
            ['Вывоз КГМ и строительных материалов 8м³ 5т IV-V класс опасности', 9500],
            ['Вывоз КГМ и строительных материалов 10м³ 7т IV-V класс опасности', 11500],
            ['Вывоз КГМ и строительных материалов 20м³ 7т IV-V класс опасности', 20000],
            ['Вывоз КГМ и строительных материалов 27м³ 10т IV-V класс опасности', 24500],
            ['Вывоз КГМ и строительных материалов 32м³ 12т IV-V класс опасности', 26500],
            ['Вывоз смесь ТКО и КГМ (микс) 27м³ 10т IV-V класс опасности', 29500],
            ['Вывоз смесь ТКО и КГМ (микс) 32м³ 12т IV-V класс опасности', 32000],
            ['Вывоз снега 1м³ без погрузки', 445],
            ['Вывоз снега 1м³ с механизированной погрузкой (от 300м)', 495],
            ['Вывоз самосвалом бетонных и железобетонных изделий, отходов бетона и железобетона, отходов цементно-песчаной плитки, бой кирпичной кладки от ремонта зданий', 550],
            ['Вывоз самосвалом грунта 1м³ IV-V класс опасности', 615],
            ['Аренда бункера 8м³ за 1 месяц (в случае менее пяти вывозов в месяц на каждый контейнер находящийся на объекте)', 5000],
            ['Аренда бункера 20-40м³ за 1 месяц (в случае менее четырех вывозов в месяц на каждый контейнер находящийся на объекте)', 15000],
            ['Постановка бункера 8-12м³', 3300],
            ['Постановка бункера 20-40м³', 9000],
        ];

        foreach ($data as $item) {
            Service::insert([
                'service_name' => $item[0],
                'price' => $item[1],
            ]);
        }




    }


}
