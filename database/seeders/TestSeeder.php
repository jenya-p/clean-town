<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Contract;
use App\Models\ContractInvoice;
use App\Models\News;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\OrderStatus;
use App\Models\ServiceType;
use App\Models\Staff;
use App\Models\StaffFunction;
use App\Models\StaffStatus;
use App\Models\TruckType;
use App\Models\Client;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class TestSeeder extends Seeder {


    public function run() {

        OrdersSeeder::clear();
        ServicesSeeder::clear();
        NewsAndNotificationsSeeder::clear();
        UsersNStaffSeeder::clear();

        srand(0);

        $this->call(UsersNStaffSeeder::class);
        $this->call(NewsAndNotificationsSeeder::class);
        $this->call(ServicesSeeder::class);
        $this->call(OrdersSeeder::class);

    }


}
