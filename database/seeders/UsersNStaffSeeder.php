<?php

namespace Database\Seeders;

use App\Models\Act;
use App\Models\Address;
use App\Models\ClientContact;
use App\Models\Contract;
use App\Models\ContractInvoice;
use App\Models\ContractInvoiceStatus;
use App\Models\News;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\OrderStatus;
use App\Models\ReconcileReport;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Staff;
use App\Models\StaffFunction;
use App\Models\StaffStatus;
use App\Models\TruckType;
use App\Models\Client;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UsersNStaffSeeder extends Seeder {

    var $invoiceStatusIds = [];

    public static function clear() {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('acts_to_invoice')->truncate();
        DB::table('invoices_to_contract')->truncate();
        // DB::table('contracts')->truncate();
        DB::table('user_services')->truncate();
        DB::table('order_staff')->truncate();
        DB::table('user_tokens')->truncate();
        DB::table('order_staff')->truncate();
        // DB::table('users')->truncate();
        DB::table('user_contacts')->truncate();
        DB::table('staff')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        self::clear();

        $this->invoiceStatusIds = ContractInvoiceStatus::all()->pluck('status_id')->toArray();

        srand(0);

        $this->staff();

        $this->users();

    }

    public function staff() {
        $staffFunctionIds = StaffFunction::pluck('function_id')->toArray();
        $staffStatusIds = StaffStatus::pluck('status_id')->toArray();
        $f = Factory::create('ru_RU');

        for ($i = 0; $i < 5; $i++) {
            /** @var Staff $staff */
            $staff = Staff::create([
                'status_id' => Arr::random($staffStatusIds),
                'function_id' => Arr::random($staffFunctionIds),
                'firstname' => $f->firstName,
                'lastname' => $f->lastName,
                'middlename' => 'Иванович',
                'photo' => null,
                'phone' => '7' . rand(100, 999) . rand(100, 999) . rand(10, 99) . rand(10, 99),
            ]);

            if (rand(0, 2) != 0) {
                $path = 'photo-staff/' . $staff->id;
                $staff->photo = Storage::putFile($path, resource_path('test_imgs/' . random_int(1, 2) . '.jpg'));
                $staff->save();
            }
        }
    }


    protected function users() {
        $staffIds = Staff::where('function_id', '=', 2) // Ваш перс манагер
        ->pluck('staff_id')->toArray();


        Client::all()->map(function (Client $user){
            $user->email = 'test@company' . $user->user_id .  '.ru';
            $user->phone = '791212345' . $user->user_id;
            $user->password = password_hash('1234567', PASSWORD_BCRYPT, [
                'cost' => 10,
            ]);

            $user->balance = rand(0,4) * 1000;
            $user->image = Storage::putFile('user-photo/1', resource_path('test_imgs/' . random_int(1, 2) . '.jpg'));
            $user->save();

            $this->contacts($user);
            $this->contractInvoicesAndActs($user, $user->user_id == 12 ? 1 : ($user->user_id == 13 ? 50 : rand(0, 5)));

        });



    }

    public function contacts(Client $user){

         $f = Factory::create('ru_RU');

        for ($i = rand(0, 2); $i > 0; $i--){
            ClientContact::create([
                'user_id' => $user->user_id,
                'position' => $f->word,
                 'name' => $f->firstName . ' ' . $f->lastName,
                'phone' => '7' . rand(100, 999) . rand(100, 999) . rand(10, 99) . rand(10, 99),
                'mobile_app_access' =>  rand(0, 1),
                'email' =>  $f->email,
                'telegram' => rand(0, 2) ? $f->word: null,
                'whatsapp' => rand(0, 2) ? $f->word: null,
            ]);
        }
    }


    protected function contractInvoicesAndActs(Client $user, $invoiceCount){

        for ($i= 0; $i < $invoiceCount; $i ++){
            $invoice = ContractInvoice::create([
                'contract_id' => $user->contract->contract_id,
                'document_date' => now()->addDays(rand(-50,50)),
                'download_link' => '/test.pdf',
                'total' => rand(1,100) * 100,
                'status_id' => Arr::random($this->invoiceStatusIds)
            ]);

            for($j = 0; $j < rand(0, 2); $j++){
                $act = Act::create([
                    'invoice_id' => $invoice->invoice_id,
                    'document_date' => now()->addDays(rand(-50,50)),
                    'download_link' => '/test.pdf',
                ]);
            }
        }
    }

    protected function reconcileReports($userId, $count){
        for ($i= 0; $i < $count; $i ++){
            $report = ReconcileReport::create([
                'user_id' => $userId,
                'document_date' => now()->addDays(rand(-50,50)),
                'download_link' => '/test.pdf',
            ]);
        }
    }

}
