<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Contract;
use App\Models\ContractInvoice;
use App\Models\News;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\OrderStatus;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\Staff;
use App\Models\StaffFunction;
use App\Models\StaffStatus;
use App\Models\TruckType;
use App\Models\Client;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class OrdersSeeder extends Seeder {

    var $userServiceIds = null;
    var $serviceTypeId = null;
    var $truckTypeIds = null;
    var $objectIds = null;
    var $statusIds = null;
    var $staffIds = null;


    public static function clear() {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('order_photos')->truncate();
        DB::table('order_staff')->truncate();
        DB::table('order_history')->truncate();
        DB::table('orders')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        self::clear();

        srand(0);

        // $this->userServiceIds = DB::table('user_services')->select()->get()->toArray();

        $this->userServiceIds = DB::select('SELECT c.user_id, ts.id as annex_service_id, ts.service_id
            FROM contracts c
            INNER JOIN contract_annex_template ct ON ct.contract_id = c.contract_id
            INNER JOIN annex_template_service ts ON ts.annex_template_id = ct.annex_template_id
        union
        SELECT o.user_id, ts.id as annex_service_id, ts.service_id
            FROM objects o
            INNER JOIN object_annex_template ot ON ot.object_id = o.object_id
            INNER JOIN annex_template_service ts ON ts.annex_template_id = ot.annex_template_id;');
        $this->serviceTypeId = ServiceType::pluck('service_type_id')->toArray();
        $this->truckTypeIds = TruckType::pluck('truck_type_id')->toArray();
        $this->objectIds = Address::pluck('object_id')->toArray();
        $this->statusIds = OrderStatus::pluck('status_id')->toArray();
        $this->staffIds = Staff::pluck('staff_id')->toArray();

        for ($i = 1; $i < 85; $i++) {
            $this->order();
        }


    }

    public function order() {

        $f = Factory::create('ru_RU');

        $usIds = Arr::random($this->userServiceIds);
        $order = Order::create([
            'user_id' => $usIds->user_id,
            'object_id' => Arr::random($this->objectIds),
            'annex_service_id' => $usIds->annex_service_id,
            'service_id' => $usIds->service_id,
            'service_type_id' => rand(0, 3) == 0 ? null : Arr::random($this->serviceTypeId),
            'truck_type_id' => rand(0, 1) == 0 ? null : Arr::random($this->truckTypeIds),
            'container_amount' => random_int(0, 100),
            'loading_required' => random_int(0, 1),
            'date_added' => now(),
            'date_panning_start' => now()->addHours(random_int(-100, 300)),
            'user_comment' => $f->text(200),
            'order_status_id' => Arr::random($this->statusIds),
        ]);

        $path = 'order-photo/' . $order->order_id;

        $path = Storage::putFile($path, resource_path('test_imgs/' . random_int(1, 2) . '.jpg'));

        OrderPhoto::create([
            'path' => $path,
            'order_id' => $order->order_id
        ]);

        foreach (Arr::random($this->staffIds, rand(0, 2)) as $staffId) {
            DB::table('order_staff')->insertOrIgnore(['order_id' => $order->order_id, 'staff_id' => $staffId]);
        }

    }


}
