<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersName extends Migration {

    public function up() {
        Schema::table('users', function (Blueprint $table) {

            $table->string('name', 256)->nullable()->after('password');

        });
    }


    public function down() {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('name');

        });
    }
}
