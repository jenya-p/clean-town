<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TruckTypesPrice extends Migration {

    public function up() {
        Schema::table('truck_types', function (Blueprint $table) {

            $table->decimal('price', 10, 2)->default(0);
        });

    }


    public function down() {
        Schema::table('truck_types', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}
