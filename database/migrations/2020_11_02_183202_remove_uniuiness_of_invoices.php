<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveUniuinessOfInvoices extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::dropIfExists('contract_invoice_statuses');
        Schema::dropIfExists('contract_invoices');

        DB::statement('ALTER TABLE `invoices_to_contract`
            DROP INDEX `UK_invoices_to_contract_contract_id`,
            ADD INDEX `UK_invoices_to_contract_contract_id` (`contract_id`) USING BTREE');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        DB::statement('ALTER TABLE `invoices_to_contract`
            DROP INDEX `FK_invoices_to_contract_status`,
            ADD UNIQUE INDEX `FK_invoices_to_contract_status` (`status_id`) USING BTREE');

    }
}
