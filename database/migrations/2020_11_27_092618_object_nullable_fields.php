<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ObjectNullableFields extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement("ALTER TABLE `objects`
        CHANGE COLUMN `country` `country` VARCHAR(255) NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `contract_id`,
        CHANGE COLUMN `region` `region` VARCHAR(255) NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `country`,
        CHANGE COLUMN `city` `city` VARCHAR(255) NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `region`,
        CHANGE COLUMN `street` `street` VARCHAR(255) NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `city`,
        CHANGE COLUMN `building_no` `building_no` VARCHAR(255) NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `street`");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement("ALTER TABLE `objects`
        CHANGE COLUMN `country` `country` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `contract_id`,
        CHANGE COLUMN `region` `region` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `country`,
        CHANGE COLUMN `city` `city` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `region`,
        CHANGE COLUMN `street` `street` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `city`,
        CHANGE COLUMN `building_no` `building_no` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_general_ci' AFTER `street`");
    }
}
