<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StaffEmail extends Migration {
    public function up() {
        Schema::table('staff', function (Blueprint $table) {

            $table->string('email', 128)->nullable()->after('phone');
        });
    }


    public function down() {
        Schema::table('staff', function (Blueprint $table) {
            $table->dropColumn('email');
        });
    }
}
