<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServicePrice extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('services', function (Blueprint $table) {
            $table->decimal('price', 10, 2)->default(0);
        });

        Schema::table('truck_types', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('truck_types', function (Blueprint $table) {
            $table->decimal('price', 10, 2)->default(0);
        });

        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }


}
