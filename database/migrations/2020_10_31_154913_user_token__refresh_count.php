<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserTokenRefreshCount extends Migration {
    public function up() {
        Schema::table('user_tokens', function (Blueprint $table) {

            $table->unsignedSmallInteger('refresh_count')->default(0)->after('abilities');

        });
    }


    public function down() {
        Schema::table('user_tokens', function (Blueprint $table) {

            $table->dropColumn('refresh_count');

        });
    }
}
