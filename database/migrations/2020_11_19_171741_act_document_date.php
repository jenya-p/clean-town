<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActDocumentDate extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        DB::statement("ALTER TABLE `acts_to_invoice`
            DROP FOREIGN KEY `FK_acts_to_invoice_invoice_id`;");
        DB::statement("ALTER TABLE `acts_to_invoice`
            ADD CONSTRAINT `FK_acts_to_invoice_invoice_id`
            FOREIGN KEY (`invoice_id`) REFERENCES `invoices_to_contract` (`invoice_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;");


        Schema::table('acts_to_invoice', function (Blueprint $table) {
            $table->date('document_date')->after('document_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        DB::statement("ALTER TABLE `acts_to_invoice`
            DROP FOREIGN KEY `FK_acts_to_invoice_invoice_id`;");
        DB::statement("ALTER TABLE `acts_to_invoice`
            ADD CONSTRAINT `FK_acts_to_invoice_invoice_id`
            FOREIGN KEY (`invoice_id`) REFERENCES `invoices_to_contract` (`contract_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;");

        Schema::table('acts_to_invoice', function (Blueprint $table) {
            $table->dropColumn('document_date');
        });
    }
}
