<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StaffExtFields extends Migration
{
    public function up() {
        Schema::table('staff', function (Blueprint $table) {
            $table->string('telegram', 128)->nullable()->after('phone');
            $table->string('whatsapp', 128)->nullable()->after('telegram');
        });
    }


    public function down() {
        Schema::table('staff', function (Blueprint $table) {
            $table->dropColumn('telegram');
            $table->dropColumn('whatsapp');
        });
    }
}
