<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServicesToServiceTypes extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('CREATE TABLE `services_to_service_types` (
    `service_id` TINYINT(4) NOT NULL,
	`service_type_id` TINYINT(4) NOT NULL,
	PRIMARY KEY (`service_type_id`, `service_id`) USING BTREE,
	CONSTRAINT `FK_services_to_service_types_service_type_id` FOREIGN KEY (`service_type_id`)
		REFERENCES `service_types` (`service_type_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK_services_to_service_types_service_id` FOREIGN KEY (`service_type_id`)
		REFERENCES `services` (`service_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COMMENT=\'определяет доступные варианты типов услуг для каждой цслуги\';');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('DROP TABLE services_to_service_types;');
    }
}
