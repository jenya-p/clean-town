<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersPersonalManager extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function (Blueprint $table) {

            $table->integer('personal_manager_id')->nullable()->after('status');
            $table->foreign('personal_manager_id', 'users__personal_manager')
                ->on('staff')
                ->references('staff_id')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users__personal_manager');
            $table->dropColumn('personal_manager_id');
        });
    }
}
