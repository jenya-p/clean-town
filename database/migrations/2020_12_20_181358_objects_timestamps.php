<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ObjectsTimestamps extends Migration {

    public function up() {
        Schema::table('objects', function (Blueprint $table) {
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('UPDATE objects SET created_at = NOW(), updated_at = NOW()');

    }

    public function down() {
        Schema::table('objects', function (Blueprint $table) {
            $table->dropTimestamps();
            $table->dropSoftDeletes();
        });
    }
}
