<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTokensTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('user_tokens', function (Blueprint $table) {
            $table->renameColumn('token_id', 'token');
        });
        Schema::table('user_tokens', function (Blueprint $table) {

            $table->string('token', 64)->change();
            $table->text('abilities')->nullable()->after('token');
            $table->timestamp('last_used_at')->nullable()->after('abilities');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::table('user_tokens')->truncate();
        Schema::table('user_tokens', function (Blueprint $table) {
            $table->renameColumn('token', 'token_id');
        });
        Schema::table('user_tokens', function (Blueprint $table) {

            $table->string('token_id', 36)->change();
            $table->dropColumn('abilities');
            $table->dropColumn('last_used_at');
            $table->dropTimestamps();

        });
    }
}
