<?php

use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderDateTime extends Migration {

    public function up() {
        // DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('orders', function (Blueprint $table) {

            $table->date('date')->after('date_added')->nullable();
            $table->enum('time', [array_keys(Order::TIME_OPTIONS)])
                ->after('date')
                ->default(Order::TIME_ANYTIME);

        });

        DB::statement("UPDATE orders SET `date` = DATE(orders.date_panning_start);");

        DB::statement("ALTER TABLE `orders`
            CHANGE COLUMN `date_panning_start` `date_panning_start` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `date_added`;");

        Schema::table('orders', function (Blueprint $table) {
            $table->date('date')->after('date_added')->change();
        });


    }


    public function down() {
        // DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        Schema::table('orders', function (Blueprint $table) {

            $table->dropColumn('date');
            $table->dropColumn('time');

        });

        DB::statement("ALTER TABLE `orders`
            CHANGE COLUMN `date_panning_start` `date_panning_start` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `date_added`;");


    }
}
