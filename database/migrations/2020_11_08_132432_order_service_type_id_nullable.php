<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderServiceTypeIdNullable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('ALTER TABLE `orders`
	CHANGE COLUMN `service_type_id` `service_type_id` TINYINT(4) NULL AFTER `service_id`;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement("
            ALTER TABLE `orders`
                DROP FOREIGN KEY `FK_orders_service_type_id`");
        DB::statement("
            ALTER TABLE `orders`
                CHANGE COLUMN `service_type_id` `service_type_id` TINYINT(4) NOT NULL AFTER `service_id`");
        DB::statement("
            ALTER TABLE `orders`
                ADD CONSTRAINT `FK_orders_service_type_id`
                FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
        ");


    }
}
