<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderServiceIdNullable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        DB::statement('ALTER TABLE `orders`
	        CHANGE COLUMN `service_id` `service_id` TINYINT(4) NULL DEFAULT NULL AFTER `object_id`');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {

        DB::statement("
            ALTER TABLE `orders`
                DROP FOREIGN KEY `FK_orders_user_id+service_id`");
        DB::statement("
            ALTER TABLE `orders`
                CHANGE COLUMN `service_id` `service_id` TINYINT(4) NOT NULL AFTER `object_id`");
        DB::statement("
            ALTER TABLE `orders`
                ADD CONSTRAINT `FK_orders_user_id+service_id`
                FOREIGN KEY (`user_id`, `service_id`) REFERENCES `user_services` (`user_id`, `service_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
        ");
    }
}
