<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserContacts extends Migration {
    public function up() {
        Schema::table('users', function (Blueprint $table) {

            $table->text('contacts')->nullable()->after('status');

        });
    }


    public function down() {
        Schema::table('users', function (Blueprint $table) {

            $table->dropColumn('contacts');

        });
    }
}
