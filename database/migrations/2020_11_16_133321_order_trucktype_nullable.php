<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderTrucktypeNullable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {


        DB::statement('DELETE FROM order_photos WHERE order_id IN (SELECT order_id FROM orders WHERE orders.service_id IS NULL);');
        DB::statement('DELETE FROM orders WHERE orders.service_id IS NULL;');

        DB::statement("
            ALTER TABLE `orders`
                DROP FOREIGN KEY `FK_orders_user_id+service_id`");
        DB::statement("
            ALTER TABLE `orders`
                CHANGE COLUMN `service_id` `service_id` TINYINT(4) NOT NULL AFTER `object_id`");
        DB::statement("
            ALTER TABLE `orders`
                ADD CONSTRAINT `FK_orders_user_id+service_id`
                FOREIGN KEY (`user_id`, `service_id`) REFERENCES `user_services` (`user_id`, `service_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
        ");


        DB::statement('ALTER TABLE `orders`
	        CHANGE COLUMN `truck_type_id` `truck_type_id` TINYINT(4) NULL DEFAULT NULL');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        DB::statement('ALTER TABLE `orders`
	        CHANGE COLUMN `service_id` `service_id` TINYINT(4) NULL DEFAULT NULL AFTER `object_id`');


        DB::statement('DELETE FROM order_photos WHERE order_id IN (SELECT order_id FROM orders WHERE orders.truck_type_id IS NULL);');
        DB::statement('DELETE FROM orders WHERE orders.truck_type_id IS NULL;');

        DB::statement("
            ALTER TABLE `orders`
                DROP FOREIGN KEY `FK_orders_truck_type_id`");
        DB::statement("
            ALTER TABLE `orders`
                CHANGE COLUMN `truck_type_id` `truck_type_id` TINYINT(4) NOT NULL");
        DB::statement("
            ALTER TABLE `orders`
                ADD CONSTRAINT `FK_orders_truck_type_id`
                FOREIGN KEY (`truck_type_id`) REFERENCES `truck_types` (`truck_type_id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
        ");

    }
}
