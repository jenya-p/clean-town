<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReconcileReportsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('reconcile_reports', function (Blueprint $table) {

            $table->id('report_id');
            $table->foreignIdFor(User::class, 'user_id');

            $table->string('document_number', 24);
            $table->date('document_date');
            $table->string('download_link', 255);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('reconcile_reports');
    }
}
