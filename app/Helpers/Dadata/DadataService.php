<?php
namespace App\Helpers\Dadata;

class DadataService{

	const DOMAIN = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/';

	protected $curl;

	public function __construct(){

        $this->curl = curl_init();
        curl_setopt_array($this->curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Accept: application/json',
                'Authorization: Token ' . env('DADATA_API_KEY')
            ]
        ]);
	}

	/**
	 * @param string $method
	 * @param array $params
	 * @return \stdClass
	 */
	public function query(string $method, array $params) : array
	{

		curl_setopt_array($this->curl, [
			CURLOPT_URL => self::DOMAIN . $method,
			CURLOPT_POSTFIELDS => json_encode($params)
		]);

		$json = curl_exec($this->curl);
		$json = json_decode($json);
		$json = $json->suggestions;
		return $json ?: [];

	}

    /**
     * @param string $location
     * @param string $region
     */
    public function getInfoByAddress(string $addr, array $location): array
    {

        $data = $this->query('suggest/address', [
            'query' => $addr,
            'locations' => $location
        ]);

        return $data;

    }

}
