<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\OrderPhoto;
use App\Models\Staff;
use App\Models\Client;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function orderPhoto(OrderPhoto $orderPhoto){
        return response()->file(storage_path('app/' . $orderPhoto->path));
    }

    public function userPhoto(Client $user){
        return response()->file(storage_path('app/' . $user->image));
    }

    public function staffPhoto(Staff $staff){
        return response()->file(storage_path('app/' . $staff->photo));
    }

    public function newsMedia(News $news){
        return response()->file(storage_path('app/' . $news->media_path));
    }


}
