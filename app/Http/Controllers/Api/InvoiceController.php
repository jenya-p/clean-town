<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\InvoiceListItem;
use App\Http\Resources\OrderListItem;
use App\Models\ContractInvoice;
use App\Models\Order;
use App\Models\Client;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{

    const IPP = 5;

    public function index(Request $request){

        /** @var Client $user */
        $user = \Auth::user();
        if(empty($user->contract)){
            return [];
        }

        $query = ContractInvoice::query()->where('contract_id', '=', $user->contract->contract_id)
            ->orderBy('invoice_id', 'desc');

        if($request->has('page')){
            $items = $query->paginate(self::IPP);
        } else {
            $items = $query->get();
        }

        return InvoiceListItem::collection($items);

    }

    public function store(Request $request){
        $this->validate($request, [
            'sum' => 'required|numeric|min:0'
        ]);

        /** @var ContractInvoice $invoice */
        $invoice = ContractInvoice::create([
            'total' => $request->sum
        ]);

        $invoice->refresh();

        return $invoice;

    }
}
