<?php
namespace App\Http\Controllers\Api;

use App\Attachment;
use App\Http\Requests\OrderRequest;
use App\Http\Resources\OrderDetails;
use App\Http\Resources\OrderListItem;
use App\Models\Address;
use App\Models\AnnexTemplateService;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\OrderStatus;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\TruckType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;


class OrderController extends BaseController {

    const IPP = 5;

    public function index(Request $request){

        $query = Order::query()->where('user_id', '=', \Auth::id())
            ->orderBy('order_id', 'desc');

        if($request->status == 'active'){
            $query->whereIn('order_status_id', [1,2]);
        } else if($request->status == 'archive'){
            $query->whereIn('order_status_id', [3,4]);
        }

        if($request->has('page')){
            $items = $query->paginate(self::IPP);
        } else {
            $items = $query->get();
        }

        return OrderListItem::collection($items);

    }

    public function show(Order $order){
        return new OrderDetails($order);
    }

    public function store(OrderRequest $request){

        $order = new Order($request->getData());
        $order->user_id = \Auth::id();

        $order->annex_service_id = AnnexTemplateService::findAnnexId($order->user_id, $order->service_id, $order->object_id);

        $order->date_added = Carbon::now();

        $orderStatus = OrderStatus::find(OrderStatus::ID__INITIAL);
        $order->order_status_id = $orderStatus->status_id;

        $order->save();

        $this->saveOrderPhoto($request, $order);

        $order->refresh();

        return new OrderDetails($order);
    }

    public function update(Order $order, OrderRequest $request){

        $order->update($request->getData());
        $order->annex_service_id = AnnexTemplateService::findAnnexId($order->user_id, $order->service_id, $order->object_id);

        $order->save();

        $this->saveOrderPhoto($request, $order);

        $order->refresh();

        return new OrderDetails($order);
    }




    public function saveOrderPhoto(OrderRequest $request, Order $order){
        $file = $request->file('photo');

        if(!empty($file)){
            $path = 'order-photo/' . $order->order_id;
            $path = Storage::put($path, $file);

            $orderPhoto = new OrderPhoto([
                // 'name' => $file->getClientOriginalName(),
                'path' => $path,
                'order_id' => $order->order_id
            ]);
            $orderPhoto->save();
            return $orderPhoto;
        }

        return null;
    }



    public function destroy(Order $order){
        $order->order_status_id = OrderStatus::ID__FINISH;
        $order->save();
    }

}
