<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceListItem;
use App\Http\Resources\StaffDetails;
use App\Models\AnnexTemplate;
use App\Models\Contract;
use App\Models\Service;
use App\Models\TruckType;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller
{


    public function index(Request $request){
        /** @var Client $user */
        $user = \Auth::user();

        $contractId = \Auth::user()->contract->contract_id;
        $annexId = DB::table('contract_annex_template')
            ->where('contract_id', '=', $contractId)
            ->where('date_start', '<=', date("Y-m-d", time()))
            ->orderBy('date_start', 'DESC')->value('annex_template_id');

        /** @var AnnexTemplate $annex */
        $annex = AnnexTemplate::with('services')->find($annexId);

        $data = [];

        foreach ($annex->services as $service){
            $data[] = [
                'id' => $service->service_id,
                'name' => $service->service_name,
                'service_price' => $service->price,
                'price' => $service->contract_price,
                'available' => $service->available,
                'types' => $service->serviceTypes()->pluck('service_type_name')
            ];
        }

        return [
            'manager' => empty($user->personal_manager) ? null: new StaffDetails($user->personal_manager),
            'data' => $data
        ];

    }

}
