<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Dadata\DadataService;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\ContractInvoice;
use App\Models\OrderStatus;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AddressesController extends Controller {


    public function store(Request $request) {
        $this->validate($request, [
            'country' => 'nullable|string|max:255',
            'region' => 'nullable|string|max:255',
            'city' => 'nullable|string|max:255',
            'street' => 'nullable|string|max:255',
            'building_no' => 'nullable|string|max:255',
            'object_name' => 'nullable|string|max:255',
        ]);

        /** @var Address $address */
        $address = new Address($request->all());
        $address->user_id = \Auth::id();
        $address->contract_id = \Auth::user()->contract->contract_id;


        $address->save();
        $address->refresh();

        return $address;
    }

    public function destroy(Address $address){
        if($address->user_id != \Auth::id()){
            abort(404);
            return;
        }

        if($address->orders()
            ->where('order_status_id', '!=', OrderStatus::ID__FINISH)
            ->exists()){
            return response('Can\'t delete this address, because there are active orders', 423);
        }

        $address->delete();
    }

    public function suggest(Request $request){
        return app()->get(DadataService::class)->getInfoByAddress($request->get('query'), ['country' => 'Россия']);
    }

}
