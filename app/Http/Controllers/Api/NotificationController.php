<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsDetails;
use App\Http\Resources\NewsListItem;
use App\Models\News;
use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    const IPP = 10;

    public function index(Request $request){
        $query = Notification::query()->orderByDesc('created_at');

        if($request->has('page')){
            $items = $query->paginate(self::IPP)->items();
        } else {
            $items = $query->get();
        }

        return ['data' => $items];
    }

    public function show(Notification $notification){
        return $notification;
    }
}
