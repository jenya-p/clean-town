<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NewsDetails;
use App\Http\Resources\NewsListItem;
use App\Http\Resources\OrderListItem;
use App\Models\News;
use App\Models\Order;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    const IPP = 10;

    public function index(Request $request){
        $query = News::query()->orderByDesc('created_at');

        if($request->has('page')){
            $items = $query->paginate(self::IPP);
        } else {
            $items = $query->get();
        }

        return NewsListItem::collection($items);
    }

    public function show(News $news){
        return new NewsDetails($news);
    }

}
