<?php
namespace App\Http\Controllers\Api;

use App\Http\Requests\OrderRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\OrderDetails;
use App\Http\Resources\OrderListItem;
use App\Http\Resources\UserDetails;
use App\Http\Resources\UserListItem;
use App\Models\Address;
use App\Models\OrderPhoto;
use App\Models\OrderStatus;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\TruckType;
use App\Models\Client;
use App\Models\UserToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class UserController extends BaseController {

    const IPP = 5;


    public function login(Request $request){

        $request->validate([
            'login' => 'required|string|max:255|min:5',
            'password' => 'required|string|min:3',
        ]);

        if(
            Auth::attempt(['email' => $request->login, 'password' => $request->password], false) ||
            Auth::attempt(['phone' => $request->login, 'password' => $request->password], false)
        ){
            $user = Auth::user();
            $token = $user->tokens()->first();

            if(!empty($token)){
                $token->delete();
                $token = null;
            }

            $token = $user->createToken('');

            return response()->json([
                'name' => $user->name,
                'email' => $user->email,
                'photo' => '',
                'position' => '',
                'int' => $user->balance,
                'token' => "Bearer ".$token->plainTextToken
            ]);

        } else {
            return response('', 404);
        }

    }

    public function refreshToken(Request $request){
        $user = Auth::user();
        /** @var UserToken $token */
        $token = $user->currentAccessToken();
        $token->delete();
        $token = $user->createToken('');

        return response()->json([
            'name' => $user->email,
            'photo' => '',
            'position' => '',
            'balance' => $user->balance,
            'token' => "Bearer ".$token->plainTextToken
        ]);
    }


//    public function refreshToken(Request $request){
//
//        // Пользователь может один раз обновить свой "протухший" токен,
//        // не передавая на сервер логин и пароль
//
//        $plainToken = trim($request->bearerToken());
//        if(empty($plainToken)){
//            return response('Token not provided', 500);
//        }
//        /** @var UserToken $token */
//        $token = UserToken::findToken($plainToken);
//        if(empty($token)){
//            return response('Your token has expired (1)', 500);
//        }
//        if(empty($token->tokenable)){
//            return response('Your token has expired (2)', 500);
//        }
//
//        if($token->refreshToken()){
//            $user = $token->tokenable;
//            return response()->json([
//                'name' => $user->name,
//                'email' => $user->email,
//                'photo' => '',
//                'position' => '',
//                'balance' => $user->balance,
//                'token' => "Bearer ".$plainToken
//            ]);
//        } else {
//            return response('Your token has expired (3)', 500);
//        }
//
//    }



//    public function index(Request $request){
//
//        $query = User::query();
//
//        if($request->has('page')){
//            $items = $query->paginate(self::IPP);
//        } else {
//            $items = $query->get();
//        }
//
//        return UserListItem::collection($items);
//
//    }

    public function show(Client $user = null){
        if(empty($user)) {
            $user = Auth::user();
        }
        return new UserDetails($user);
    }

//    public function store(UserRequest $request){
//
//        $user = new User($request->getData());
//
//        $user->save();
//
//        $this->saveUserPhoto($request, $user);
//
//        $user->refresh();
//
//        return new UserDetails($user);
//    }
//
//    public function update(User $user, UserRequest $request){
//
//        $user->update($request->getData());
//
//        $user->save();
//
//        $this->saveUserPhoto($request, $user);
//
//        $user->refresh();
//
//        return new UserDetails($user);
//    }
//
//
//
//    public function saveUserPhoto(UserRequest $request, User $user){
//        $file = $request->file('photo');
//
//        if(!empty($file)){
//            $path = 'user-photo/' . $user->id;
//            $path = Storage::put($path, $file);
//
//            $userPhoto = new UserPhoto([
//                // 'name' => $file->getClientOriginalName(),
//                'path' => $path,
//                'user_id' => $user->user_id
//            ]);
//            $userPhoto->save();
//            return $userPhoto;
//        }
//
//        return null;
//    }



//    public function destroy(User $user){
//        $user->delete();
//    }




}
