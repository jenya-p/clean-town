<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ServiceListItem;
use App\Http\Resources\StaffDetails;
use App\Models\Driver;
use App\Models\Staff;
use App\Models\TruckType;
use App\Models\Client;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function index(Request $request){

        $data = [];

        /** @var Staff $staff */
        foreach (Staff::all() as $staff){
            $photoUrl = null;
            if(!empty($this->photo)){
                $photoUrl = route('photo.staff', $staff);
            }

            $data[] = [
                "staff_id" => $staff->staff_id,
                "name" => $staff->name,
                "email" => $staff->email,
                "phone" => $staff->phone,
                "telegram" => $staff->telegram,
                "whatsapp" => $staff->whatsapp,
                "photo_url" => $photoUrl,
                "position" => $staff->function->function_name,
                "status" => $staff->status->status_name
            ];
        }


        /** @var Driver $staff */
        foreach (Driver::all() as $driver){

            $data[] = [
                "staff_id" => $driver->id,
                "name" => $driver->full_name,
                "email" => null,
                "phone" => $driver->phone,
                "telegram" => null,
                "whatsapp" => null,
                "photo_url" => null,
                "position" => 'Водитель',
                "status" => 'Онлайн'
            ];
        }


        return [
            'data' => $data
        ];

    }
}
