<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ReconcileReport;

class ReconcileReportController extends Controller
{
    public function store(){

        /** @var ReconcileReport $report */
        $report = ReconcileReport::create([
            'download_link' => '/test.pdf'
        ]);

        $report->refresh();

        return $report;

    }
}
