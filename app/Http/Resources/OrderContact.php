<?php

namespace App\Http\Resources;

use App\Models\Staff;
use App\Models\Client;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderContact extends JsonResource {

    public static $wrap = false;

    public function toArray($request) {
        /** @var Staff $this */

        $data = [
            'staff_id' => $this->staff_id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'photo_url' => $this->photo_url, //? url ? base64
            'position' => $this->function ? $this->function->function_name : '',
            'status' => $this->status ? $this->status->status_name : '',
        ];

        return $data;

    }
}
