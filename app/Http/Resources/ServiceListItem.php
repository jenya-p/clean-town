<?php

namespace App\Http\Resources;

use App\Models\Service;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceListItem extends JsonResource {

    public static $wrap = false;

    public function toArray($request) {
        /** @var Service $this   */

        return [
            'id' => $this->service_id,
            'name' => $this->service_name,
            'price' => $this->price,
            'available' => $this->available,
            'types' => $this->serviceTypes()->pluck('service_type_name')
        ];

    }
}
