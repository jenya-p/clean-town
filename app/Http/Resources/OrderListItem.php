<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;



class OrderListItem extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request) {
        /** @var Order $this  */
        return [
            'id' => $this->order_id,
            'status' => $this->order_status_id,
            'date_added' => $this->date_added,
            'date' => (!empty($this->date)) ? $this->date->format('Y-m-d'): null,
            'time' => $this->time,
            'date_from' => $this->date_panning_start,
            'date_to' => $this->date_panning_end,
            'address' => $this->object
        ];
    }
}
