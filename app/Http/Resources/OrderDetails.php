<?php

namespace App\Http\Resources;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetails extends JsonResource
{

    public static $wrap = false;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $service = null;
        if(!empty($this->annexService)){
            $service = $this->annexService->service;
            $service->base_price = $service->price;
            $service->price = $this->annexService->contract_price;
        };

        /** @var Order $this */
        return [
            'id' => $this->order_id,
            'status' => $this->order_status_id,
            'address' => $this->object,
            'service' => $service,
            'volume' => empty($service) ? null:  $service->service_name,
            'service_type' =>   empty($this->serviceType) ? null:   $this->serviceType->service_type_name,
            'truck_type' =>     empty($this->truckType) ? null:     $this->truckType->truck_type_name,
            'route' => $this->getRoute(),
            'date' => (!empty($this->date)) ? $this->date->format('Y-m-d'): null,
            'time' => $this->time,
            'arrive_time' => now()->diffInMinutes($this->date_panning_start), // минут до прибытия
            'date_from' => $this->date_panning_start,
            'date_to' => $this->date_panning_end,
            'contacts' => OrderContact::collection($this->contacts),

            'date_added' => $this->date_added,
        ];

    }
}
