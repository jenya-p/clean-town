<?php

namespace App\Http\Resources;

use App\Models\News;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsDetails extends JsonResource {

    public static $wrap = false;

    public function toArray($request) {
        /** @var News $this   */

      return [
            'id' => $this->id,
            'title' => $this->title,
            'text' => $this->text,
            'media_url' => $this->media_url,
            'is_video' => $this->is_video,
            'news_url' => $this->url,
      ];

    }
}
