<?php

namespace App\Http\Resources;

use App\Models\ContractInvoice;
use App\Models\News;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceListItem extends JsonResource {

    public static $wrap = false;

    public function toArray($request) {

        /** @var ContractInvoice $this */

        return [
            'invoice_id' => $this->invoice_id,
            'document_number' => $this->document_number,
            'document_date' => $this->document_date,
            'name' => $this->name,
            'acts' => $this->acts,
            'total' => $this->total,
            'download_link' => $this->download_link,
            'was_paid' => $this->was_paid,
        ];
    }
}
