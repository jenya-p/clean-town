<?php

namespace App\Http\Resources;

use App\Models\Client;
use Illuminate\Http\Resources\Json\JsonResource;

class UserDetails extends JsonResource {

    public static $wrap = false;

    const MAX_INVOICE_COUNT = 2;

    public function toArray($request) {
        /** @var Client $this */

        $invoiceCount = $this->invoices()->count();
        $invoices = $this->invoices()->orderBy('invoice_id', 'ASC')
            ->offset($invoiceCount - self::MAX_INVOICE_COUNT)->limit(self::MAX_INVOICE_COUNT)->get();

        $data = [
            'name' => $this->name,
            'email' => $this->email,
            'photo' => $this->photo_url,
            'legal_type' => $this->legal_type,
            'legal_type_name' => $this->legal_type == 1 ? 'Физическое лицо': 'Юридическое лицо',
            'balance' => $this->balance,        // балланс
            'addresses' => $this->addresses,    // адреса пользователя
            'invoices' => InvoiceListItem::collection($invoices),      // счета на оказание услуг
            'contract_name' => null,
            'contract_url' => null,
            'contacts' => $this->contact,      //String; // Реквизиты компании оказывающие услуги
            'reconcile_report' => $this->lastReconcileReport
        ];

        if(!empty($this->contract)){
            $data['contract_name'] = $this->contract->document_number;  //String; // Договор на оказание услуг №352
            $data['contract_url'] = $this->contract->download_link;   //String; // ссылка для скачивания договора
            $data['contract'] = $this->contract;
        }

        return $data;

    }
}
