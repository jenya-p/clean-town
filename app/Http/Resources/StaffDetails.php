<?php

namespace App\Http\Resources;

use App\Models\News;
use App\Models\Staff;
use App\Models\TruckType;
use Illuminate\Http\Resources\Json\JsonResource;

class StaffDetails extends JsonResource {

    public static $wrap = false;

    public function toArray($request) {
        /** @var Staff $this   */

        $photoUrl = null;
        if(!empty($this->photo)){
            $photoUrl = route('photo.staff', $this);
        }

        return [
            "staff_id" => $this->staff_id,
            "name" => $this->name,
            "email" => $this->email,
            "phone" => $this->phone,
            "telegram" => $this->telegram,
            "whatsapp" => $this->whatsapp,
            "photo_url" => $photoUrl,
            "position" => $this->function->function_name,
            "status" => $this->status->status_name
        ];

    }
}
