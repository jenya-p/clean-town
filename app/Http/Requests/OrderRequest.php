<?php

namespace App\Http\Requests;

use App\Models\Order;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\TruckType;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;

/**
 *
 * @property string category
 * @property string service_type
 * @property string volume
 * @property int count
 * @property string address
 * @property string object
 * @property string date_time
 * @property bool loading_required
 * @property string comment
 * @property string photo
 *
 * Class OrderRequest
 * @package App\Http\Requests
 */
class OrderRequest extends FormRequest {


    public function rules() {
        return [
            'service_id' =>      'required|integer|exists:services,service_id',
            'type'       =>      ['nullable','string',
                                    function ($attribute, $value, $fail) {
                                        if(empty($this->type)) {return null;}
                                        /** @var ServiceType $serviceType */
                                        $serviceType = ServiceType::where(['service_type_name' => $this->type])->first();
                                        if(empty($serviceType)){
                                            return $fail($attribute. ' not found');
                                        }
                                        /** @var Service $service */
                                        $service = Service::find($this->service_id);
                                        if(!$service->serviceTypes->contains($serviceType->service_type_id)){
                                            return $fail($attribute. ' not available for provided service');
                                        }
                                }],
            'address_id' =>      'required|integer|exists:objects,object_id',
            'count' =>           'required|integer|min:0', // Количество
            'date' =>            'required|date_format:"Y-m-d"', // String; // YYYY-MM-DD
            'time' =>            ['required', Rule::in(array_keys(Order::TIME_OPTIONS))],
            'loading_required' =>'nullable|boolean', // Требуется погрузка
            'comment' =>         'nullable|string', // Комментарии
            'photo' =>           'nullable|file'
        ];
    }

    public function getData(){

        /** @var ServiceType $serviceType */
        if(!empty($this->type)){
            $serviceType = ServiceType::where(['service_type_name' => $this->type])->firstOrFail();
            $serviceTypeId = $serviceType->id;
        } else {
            $serviceTypeId = null;
        }

        return [
            'object_id' => $this->address_id,
            'service_type_id' => $serviceTypeId,
            'service_id' => $this->service_id,
            'container_amount' => $this->count,
            'loading_required' => (bool)$this->loading_required,
            'date' => Carbon::createFromFormat('Y-m-d', $this->date)->startOfDay(),
            'time' => $this->time,
            'user_comment' => $this->comment,
        ];

    }


}
