<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $function_id
 * @property string $function_name
 *
 * @property Staff[] $staff
 *
 * @mixin \Eloquent
 *
 */
class StaffFunction extends Model
{

    protected $primaryKey = 'function_id';
    public $timestamps = false;
    protected $fillable = ['function_name'];

    public function staff(){
        return $this->hasMany(Staff::class, 'function_id', 'function_id');
    }

}
