<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * @property int $id
 * @property int $user_id
 * @property string $position
 * @property string $name
 * @property string $phone
 * @property string $mobile_app_access
 * @property string $email
 * @property string $telegram
 * @property string $whatsapp
 *
 *
 * @property-read ServiceType[] $serviceTypes
 *
 * @mixin \Eloquent
 *
 */
class ClientContact extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $table = 'user_contacts';

    public function client(){
        return $this->belongsTo(Client::class,'user_id');
    }

}
