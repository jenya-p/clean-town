<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * @property int $contract_id
 * @property string $status_name
 *
 * @mixin \Eloquent
 */
class ContractStatus extends Model {

    const DEFAULT_STATUS_ID = 1;

    protected $primaryKey = 'status_id';
    public $timestamps = false;
    protected $fillable = ['status_name'];


}
