<?php

namespace App\Models;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
  * @property int $status_id
  * @property string $status_name
 *
 * @mixin Eloquent
 */
class ContractInvoiceStatus extends Model{

    const ID_NOT_PAID = 1;
    const ID_PAID = 2;

    protected $table = 'invoice_statuses';

    protected $primaryKey = 'status_id';

    public $timestamps = false;

    protected $fillable = ['status_name'];

}
