<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;


/**
 *
 * @property int $user_id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $phone
 * @property integer $legal_type
 * @property integer $balance
 * @property string $image
 * @property integer $status
 * @property integer $personal_manager_id
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at

 * @property-read string $contact
 * @property-read string $photo_url
 * @property-read UserToken[] $tokens
 * @property-read Service[] $services
 * @property-read Address[] $addresses
 * @property-read Contract $contract
 * @property-read ContractInvoice[] $invoices
 * @property-read Staff $personal_manager
 * @property-read ClientContact $contacts
 *
 * @property-read ReconcileReport[] $reconcileReports
 * @property-read ReconcileReport   $lastReconcileReport
 *
 * @mixin \Eloquent
 */
class Client extends Authenticatable {
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $table = 'users';

    protected $primaryKey = 'user_id';

    protected $fillable = ['email','name','password','phone','legal_type','balance','image',
        'status', 'contacts', 'personal_manager_id', 'remember_token', 'created_at', 'updated_at', 'price_agreement_protocol_date',
        'start_date', 'pay_type', 'bank_details', 'pay_type_credit_advance', 'reference_period', 'deferment', 'minimum_balance',
        'inn',
        'opf',
        'region',
        'legal_name',
        'ogrn',
        'ati_code',
        'director',
        'director_case',
        'legal_address',
        'actual_address',
        'postal_address',
        'kpp',
        'payment_account',
        'bic',
        'bank_name',
        'correspondent_account',
    ];
    protected $dates = ['created_at','updated_at','deleted_at'];



    public function getPhotoUrlAttribute(){
        return route('photo.user', $this);
    }


    public function getContactAttribute(){
        $contact = $this->contacts()->orderBy('id')->first();
        if($contact){
            return implode(' ', [$contact->name, $contact->phone]);
        } else {
            return null;
        }
    }

    public function tokens() {
        return $this->hasMany(UserToken::class, 'user_id', 'user_id');
    }

    public function services(){
        return $this->belongsToMany(Service::class, 'user_services', 'user_id', 'service_id');
    }

    public function addresses(){
        return $this->hasMany(Address::class, 'user_id', 'user_id');
    }

    public function contract(){
        return $this->hasOne(Contract::class, 'user_id', 'user_id');
    }

    public function personal_manager(){
        return $this->belongsTo(Staff::class, 'personal_manager_id', 'staff_id');
    }

    public function invoices(){
        return $this->hasManyThrough(ContractInvoice::class, Contract::class, 'user_id', 'contract_id', 'user_id', 'contract_id');
    }

    public function reconcileReports(){
        return $this->hasMany(ReconcileReport::class, 'user_id', 'user_id');
    }

    public function lastReconcileReport(){
        return $this->hasOne(ReconcileReport::class, 'user_id', 'user_id')
            ->orderBy('document_date', 'DESC')
            ->orderBy('report_id', 'DESC')
            ->take(1);
    }

    public function contacts(){
        return $this->hasMany(ClientContact::class,'user_id');
    }

}
