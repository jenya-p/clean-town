<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * @property int $truck_type_id
 * @property string $truck_type_name
 *
 * @property-read ServiceType[] $serviceTypes
 *
 * @mixin \Eloquent
 *
 */
class TruckType extends Model
{
    protected $primaryKey = 'truck_type_id';
    public $timestamps = false;
    protected $fillable = ['truck_type_name'];


    public function serviceTypes(){
        return $this->belongsToMany(ServiceType::class,
            'truck_types_to_service_types',
            'truck_type',
            'service_type_id',
            'truck_type_id',
            'service_type_id');
    }

}
