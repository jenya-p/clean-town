<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $media_path
 * @property string $media_name
 * @property string $url
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at

 * @property-read string $media_url
 * @property-read boolean $is_video
 *
 * @mixin \Eloquent
 */

class News extends Model {

    protected $table = 'news';

    protected $fillable=['id','title','text','media_path','media_name','created_at','updated_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];

    public function getMediaUrlAttribute(){
        return route('news-media', $this);
    }

    public function getIsVideoAttribute(){
        return false;
    }



}
