<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * @property int $id
 * @property int $service_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property int $annex_template_id
 * @property float $contract_price
 * @property int $sort
 * @property int $use_driver_payment
 * @property float $driver_payment
 *
 * @property-read Service $service
 * @mixin \Eloquent
 */
class AnnexTemplateService extends Model
{
    use HasFactory;
    protected $table = 'annex_template_service';
    protected $fillable = ['service_id', 'annex_template_id', 'contract_price', 'sort'];
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function service(){
        return $this->belongsTo(Service::class, 'service_id');
    }

    public static function findAnnexId($userId, $serviceId, $objectId = null){

        $sql = 'SELECT id FROM(
                SELECT ts.id, ts.service_id, NULL AS object_id
            FROM contracts c
            INNER JOIN contract_annex_template ct ON ct.contract_id = c.contract_id
            INNER JOIN annex_template_service ts ON ts.annex_template_id = ct.annex_template_id
            WHERE c.user_id = ' . $userId . ' AND ts.service_id = ' . $serviceId . '
        union
        SELECT ts.id, ts.service_id, ot.object_id
            FROM objects o
            INNER JOIN object_annex_template ot ON ot.object_id = o.object_id
            INNER JOIN annex_template_service ts ON ts.annex_template_id = ot.annex_template_id
            WHERE o.user_id = ' . $userId . ' AND ts.service_id = ' . $serviceId . '
        ) t';

        $params = [];

        if($objectId){
            $sql .= ' order by object_id = :object_id DESC';
            $params['object_id'] = $objectId;
        } else {
            $sql .= ' order by object_id is null DESC';
        }
        $sql .= ' LIMIT 1';

        $result = \DB::selectOne($sql,$params);
        if($result){
            return $result->id;
        } else {
            return null;
        }

    }

}
