<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property int    $report_id
 * @property int    $user_id
 * @property string $document_number
 * @property Carbon $document_date
 * @property string $download_link
 *
 * @property-read string $name
 * @property-read Client $user
 *
 * @mixin \Eloquent
 */
class ReconcileReport extends Model
{
    protected $table = 'reconcile_reports';

    protected $primaryKey = 'report_id';

    public $timestamps = false;

    protected $fillable = ['document_number', 'document_date', 'download_link'];

    protected $dates = ['document_date'];

    protected $appends = ['name'];

    protected static function boot() {
        parent::boot();
        self::creating(function( self $me){

            if(empty($me->user_id)){
                $me->user_id = \Auth::id();
            }
            if(empty($me->document_date)){
                $me->document_date = now();
            }
            if(empty($me->document_number)){
                $me->document_number = (self::max('report_id') + 1) . '/' . $me->document_date->year;
            }
            if(empty($me->download_link)){
                $me->download_link = '';
            }

            return $me;

        });
    }

    public function user() {
        return $this->belongsTo(Client::class, 'user_id', 'user_id');
    }

    public function getNameAttribute(){
        $name = 'Акт сверки №'.$this->document_number;
        if(!empty($this->document_date)){
            $name .= ' от '.$this->document_date->format('d.m.Y');
        }
        return $name;
    }
}
