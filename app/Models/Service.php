<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $service_id
 * @property string $service_name
 * @property float $price
 *
 * @property-read boolean $available
 * @property-read ServiceType[] $serviceTypes
 * @property-read Client[] $users
 *
 * @mixin \Eloquent
 *
 */
class Service extends Model {

    protected $primaryKey = 'service_id';
    public $timestamps = false;
    protected $fillable = ['service_name', 'price', 'is_journey'];

    public function serviceTypes(){
        return $this->belongsToMany(ServiceType::class,
            'services_to_service_types',
            'service_id',
            'service_type_id',
            'service_id',
            'service_type_id');
    }


    public function getAvailableAttribute(){
        $userId  = \Auth::id();
        if(empty($userId)) return false;

        $result = \DB::selectOne('SELECT COUNT(*) as count
	        FROM user_services us WHERE us.service_id = :service_id AND us.user_id = :user_id',
            [
                'service_id' => $this->truck_type_id,
                'user_id' => $userId
            ]);

        return $result->count > 0;
    }

    public function users(){
        return $this->belongsToMany(Client::class, 'user_services', 'service_id', 'user_id');
    }

}
