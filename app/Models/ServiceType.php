<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * @property int $service_type_id
 * @property string $service_type_name
 *
 * @mixin \Eloquent
 *
 */
class ServiceType extends Model
{
    protected $primaryKey = 'service_type_id';
    public $timestamps = false;
    protected $fillable = ['service_type_name'];
}
