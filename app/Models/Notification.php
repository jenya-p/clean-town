<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $text
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @mixin \Eloquent
 */
class Notification extends Model {

    protected $fillable = ['id', 'title', 'text', 'created_at', 'updated_at'];

    protected $hidden = ['deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

}
