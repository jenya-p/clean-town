<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 *
 * @property int    $id
 * @property string $surname
 * @property string $name
 * @property string $patronymic
 * @property string $employee_number
 * @property string $phone
 * @property string $start_date
 * @property string $end_date
 * @property string $birthdate
 * @property string $truck_id
 * @property string $stabilization_fund_max
 * @property string $photo
 * @property string $use_pay_per_trip
 * @property string $use_stabilization_fund_value
 * @property string $is_virtual
 *
 * @property-read string $full_name
 * @property-read StaffStatus $status
 * @property-read StaffFunction $function
 *
 * @mixin \Eloquent
 **/
class Driver extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = ['id','surname','name','patronymic','employee_number','phone',
        'start_date','end_date','birthdate','truck_id','stabilization_fund_max','photo',
        'use_pay_per_trip','use_stabilization_fund_value','is_virtual',
    ];

    public function getFullNameAttribute() {
        return implode(' ', [$this->name, $this->patronymic, $this->surname]);
    }
}
