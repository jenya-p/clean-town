<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

/**
 * @property int $photo_id
 * @property int $order_id
 * @property string $path
 *
 * @proerty-read string $url
 * @proerty-read string $thumbUrl
 * @proerty-read string $ext
 *
 * @property-read Order $order
 *
 * @mixin \Eloquent
 */
class OrderPhoto extends Model
{

    protected $primaryKey = 'photo_id';
    public $timestamps = false;
    protected $fillable = ['photo_id','order_id','path',];

    public static function boot() {
        parent::boot();

        self::creating(function (self $me){
            self::where(['order_id' => $me->order_id])->delete();
        });

        // TODO Удаление файлов в self::deleted();
    }

    protected $appends = ['url'];
    protected $hidden = ['path'];

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function getUrlAttribute(){
        return route('photo.order', $this);
    }

    public function getThumbUrlAttribute(){
        $ext = pathinfo($this->file, PATHINFO_EXTENSION);
        if(in_array($ext, ['jpeg', 'jpg', 'png', 'bmp'])){
            return route('order-photo', $this);
        } else if(in_array($ext, ['doc', 'docx'])){
            return '/images/icons/file_types/doc.svg';
        } else if(in_array($ext, ['xls', 'xlsx'])){
            return '/images/icons/file_types/excel.svg';
        } else if(in_array($ext, ['pdf'])){
            return '/images/icons/file_types/pdf.svg';
        } else {
            return '/images/icons/file_types/file.svg';
        }
    }

    public function getExtAttribute(){
        return \File::extension($this->file);
    }


}
