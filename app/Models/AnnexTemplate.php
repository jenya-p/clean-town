<?php

namespace App\Models;

use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AnnexTemplate
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property Carbon $created_at
 * @property Carbon $updated_at

 * @property-read Service[] $services
 * @property-read Contract $contrct
 *
 * @mixin \Eloquent
 */
class AnnexTemplate extends Model
{
    use HasFactory;
    protected $fillable = ['title'];
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function services(){
        return $this
                ->belongsToMany(Service::class, 'annex_template_service', 'annex_template_id', 'service_id')
                ->withPivot('contract_price', 'id', 'sort')->orderBy('sort','ASC');
    }


    public function contract(){
        return $this
                ->belongsToMany(Contract::class, 'contract_annex_template', 'annex_template_id', 'contract_id');
    }

}
