<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @property int $object_id
 * @property int $user_id
 * @property int $contract_id
 * @property string $country
 * @property string $region
 * @property string $city
 * @property string $street
 * @property string $building_no
 * @property string $object_name
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property-read Client $user
 * @property-read Order[] $orders
 *
 * @mixin \Eloquent
 */
class Address extends Model {

    protected $table = 'objects';
    protected $primaryKey = 'object_id';

    use SoftDeletes;

    protected $fillable = ['object_id','user_id','contract_id','country','region','city','street','building_no','object_name', 'created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function orders(){
        return $this->hasMany(Order::class, 'object_id', 'object_id');
    }

}
