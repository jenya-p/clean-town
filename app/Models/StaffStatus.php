<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * @property int $status_id
 * @property string $status_name
 *
 * @property Staff[] $staff
 *
 * @mixin \Eloquent
 *
 */
class StaffStatus extends Model
{
    protected $primaryKey = 'status_id';
    public $timestamps = false;
    protected $fillable = ['status_name'];

    public function staff(){
        return $this->hasMany(Staff::class, 'status_id', 'status_id');
    }
}
