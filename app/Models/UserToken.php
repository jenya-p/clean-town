<?php

namespace App\Models;

use App\Models\Client;
use Carbon\Carbon;
use Laravel\Sanctum\PersonalAccessToken;

/**
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property array $abilities
 * @property string $refresh_count
 * @property $last_used_at
 * @property $expiry_date
 * @property $obsolete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Client $tokenable
 *
 * @package App\Models
 */
class UserToken extends PersonalAccessToken {


    const MAX_REFRESH_TIMES = 1;

    protected $fillable = [
        'token',
        'abilities',
        'refresh_count'
    ];

    public function tokenable() {
        return $this->belongsTo(Client::class, 'user_id');
    }

    public function isObsolete(){
        $expirationMinutes = config('sanctum.expiration');
        if(empty($expirationMinutes)){
            return false;
        } else {
            return $this->created_at <= Carbon::now()->subMinutes($expirationMinutes);
        }
    }


    public function refreshToken(){

        if($this->refresh_count < self::MAX_REFRESH_TIMES){
            $this->refresh_count++;
            $this->created_at = now();
            $this->save();
            return true;
        } else {
            return false;
        }
    }

}
