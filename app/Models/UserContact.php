<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $phone
 *
 * @property-read ServiceType[] $serviceTypes
 *
 * @mixin \Eloquent
 *
 */
class UserContact extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $table = 'user_contacts';

    public function client(){
        return $this->belongsTo(Client::class,'user_id');
    }

}
