<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $staff_id
 * @property int $status_id
 * @property int  $function_id
 * @property string $firstname
 * @property string $lastname
 * @property string $middlename
 * @property string $photo
 * @property string $phone
 * @property string $email
 * @property string $telegram
 * @property string $whatsapp
 *
 * @property-read string $name
 * @property-read string $photo_url
 * @property-read StaffStatus $status
 * @property-read StaffFunction $function
 *
 * @mixin \Eloquent
 */
class Staff extends Model{
    protected $primaryKey = 'staff_id';
    public $timestamps = false;
    protected $fillable = ['status_id','function_id','firstname','lastname','middlename','photo','phone',
        'telegram',  'whatsapp',  ];

    public function status(){
         return $this->belongsTo(StaffStatus::class, 'status_id', 'status_id');
    }

    public function function(){
         return $this->belongsTo(StaffFunction::class, 'function_id', 'function_id');
    }

    public function getPhotoUrlAttribute(){
        if(!empty($this->photo)){
            return route('photo.staff', $this);
        } else {
            return null;
        }
    }

    public function getNameAttribute(){
        return implode(' ', [$this->firstname,$this->middlename,$this->lastname]);
    }

}
