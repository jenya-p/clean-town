<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $status_id
 * @property string $status_name
 *
 * @mixin \Eloquent
 *
 */
class OrderStatus extends Model {

    const ID__INITIAL = 1;
    const ID__FINISH = 4;

    protected $primaryKey = 'status_id';
    public $timestamps = false;
    protected $fillable = ['status_name'];
}
