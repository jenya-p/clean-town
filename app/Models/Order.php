<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $order_id
 * @property int $user_id
 * @property int $object_id
 * @property int $service_id
 * @property int $service_type_id
 * @property int $truck_type_id
 * @property int $container_amount
 * @property boolean $loading_required
 * @property Carbon $date_added
 * @property Carbon $date
 * @property string $time
 * @property Carbon $date_panning_start
 * @property Carbon $date_panning_end
 * @property string $user_comment
 * @property int $order_status_id
 * @property int $spoil_disposal_id
 * @property int $driver_id
 * @property int $truck_id
 * @property int $got_journey
 * @property int $annex_service_id
 *
 *
 * @property-read Address $object
 * @property-read Client $user
 * @property-read AnnexTemplateService $annexService
 * @property-read Service $service
 * @property-read ServiceType $serviceType
 * @property-read TruckType $truckType
 * @property-read OrderStatus $status
 * @property-read OrderPhoto $photos
 * @property-read Staff[] $contacts
 * @property-read Driver $driver
 *
 * @mixin \Eloquent
 */
class Order extends Model {


    protected $primaryKey = 'order_id';

    public $timestamps = false;

    protected $fillable = ['user_id','object_id','service_id','service_type_id','truck_type_id','annex_service_id',
        'container_amount','loading_required','date_added','date_panning_start', 'date_panning_end', 'date', 'time',
        'user_comment', 'order_status_id',
    ];

    protected $dates = ['date_added','date_panning_start', 'date_panning_end'];

    protected $casts = [
        'loading_required' => 'bool',
        'date' =>'datetime:Y-m-d',
    ];


    const TIME_FIRSTHALF = 'firsthalf';
    const TIME_SECONDHALF = 'secondhalf';
    const TIME_NIGHT = 'night';
    const TIME_ANYTIME = 'anytime';

    const TIME_OPTIONS = [
        self::TIME_FIRSTHALF => 'Первая половина дня',
        self::TIME_SECONDHALF => 'Вторая половина дня',
        self::TIME_NIGHT => 'Ночь',
        self::TIME_ANYTIME => 'Любое время',
        ];


    public static function boot() {
        parent::boot();

        self::deleting(function (self $me){
            $me->photos()->delete();
        });
    }


    public function user(){
        return $this->belongsTo(Client::class, 'user_id', 'user_id');
    }

    public function object(){
        return $this->belongsTo(Address::class, 'object_id');
    }

    public function annexService(){
        return $this->belongsTo(AnnexTemplateService::class, 'annex_service_id');
    }

    public function service(){
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function serviceType(){
        return $this->belongsTo(ServiceType::class, 'service_type_id');
    }

    public function truckType(){
        return $this->belongsTo(TruckType::class, 'truck_type_id');
    }

    public function photos(){
        return $this->hasMany(OrderPhoto::class, 'order_id', 'order_id');
    }

    public function status(){
        return $this->belongsTo(OrderStatus::class, 'order_status_id');
    }

    public function contacts(){
        return $this->belongsToMany(Staff::class,
            'order_staff',
            'order_id',
            'staff_id',
            'order_id',
            'staff_id');
    }

    public function getRoute(){
        return [
            [55.9729883, 37.4897566],
            [55.9809533, 37.4530433],
            [55.9813233, 37.4000016],
            [55.9743533, 37.3774583],
            [55.9835333, 37.4233416],
            [55.971435, 37.4496899],
            [55.9749283, 37.504785],
            [55.9589, 37.5331916],
            [55.9205116, 37.5442433],
            [55.9107716, 37.5736599],
            [55.8977666, 37.639805],
            [55.9085316, 37.6692433],
            [55.9278583, 37.719045],
        ];
    }

}
