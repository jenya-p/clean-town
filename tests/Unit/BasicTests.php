<?php

namespace Tests\Unit;

use App\Models\Act;
use App\Models\AnnexTemplateService;
use App\Models\ContractInvoice;
use App\Models\Order;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\TruckType;
use App\Models\Client;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertEquals;

class BasicTests extends \Tests\TestCase
{
    public function testOrderService(){
        echo AnnexTemplateService::findAnnexId(10, 34, 20); die;
    }


    public function testServicesAndUsers(){

        /** @var Service $service */
        $service = Service::find(1);
        /** @var Client $user */
        $user = Client::find(10);

        $this->assertEquals(count($service->users), 3);
        $this->assertEquals(count($user->services), 17);

        $this->assertTrue($user->services->contains($service->service_id));

        $this->assertTrue($service->users->contains($user->user_id));

        /** @var Service $service */
        $service = Service::find(7);
        /** @var Client $user */
        $user = Client::find(11);

        $this->assertFalse($user->services->contains($service->service_id));

        $this->assertFalse($service->users->contains($user->user_id));


    }


    public function testUsersContractsInvoicesActs(){

        /** @var Client $user */
        $user = Client::find(10);

        $this->assertEquals('1/2020', $user->contract->document_number);
        $this->assertEquals('Активный', $user->contract->status->status_name);

        $this->assertCount(6, $user->addresses);
        $this->assertEquals('Мавзолей', $user->addresses[0]->object_name);

        $this->assertCount(4, $user->invoices);

        $invoice = ContractInvoice::find('4');

        $this->assertEquals('4/2020', $invoice->document_number);
        $this->assertTrue($invoice->was_paid);
        $this->assertCount(2, $invoice->acts);

        $act = Act::find('4');
        $this->assertEquals('4/2020', $act->document_number);
        $this->assertEquals(4, $act->invoice->invoice_id);

        /** @var Client $user */
        $user = Client::find(11);

        $this->assertCount(1, $user->invoices);
        $this->assertEquals('5/2020', $user->invoices[0]->document_number);
        $this->assertTrue($user->invoices[0]->was_paid);

    }

    public function testUsersAndPersonalManager(){
        /** @var  $user */
        $user = Client::find(10);
        $this->assertEquals(4, $user->personal_manager_id);
        $this->assertEquals('Туров', $user->personal_manager->lastname);
    }


    public function testServices(){
        /** @var Service $service */
        $service = Service::find(3);
        $this->assertCount(3, $service->serviceTypes);

        $service->serviceTypes->map(function(ServiceType $itm){
            $this->assertTrue(in_array($itm->service_type_name, ['Замена','Снятие','Постановка']));
        });


    }


    public function testUserReconcileReports(){
        $user = Client::find(10);

        $this->assertEquals(3, count($user->reconcileReports));

        $this->assertEquals(
            $user->reconcileReports()->max('document_date'),
            $user->lastReconcileReport->document_date->format('Y-m-d')
        );

        $user = Client::find(11);

        $this->assertEquals(1, count($user->reconcileReports));

        $user = Client::find(12);

        $this->assertEmpty($user->reconcileReports);
        $this->assertNull(($user->lastReconcileReport));

    }

}
