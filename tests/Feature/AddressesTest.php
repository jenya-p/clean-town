<?php

namespace Tests\Feature;

use App\Models\Address;
use App\Models\OrderStatus;
use App\Models\Client;
use Faker\Factory;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AddressesTest extends TestCase
{

    public function testCreate()
    {
        $fk = Factory::create('ru_RU');

        $user = Client::find(10);
        Sanctum::actingAs($user);

        $count = $user->addresses->count();

        // Создаем заказ
        $response = $this->post('/v1/addresses', [
            'country' => $fk->country,
            'region' => $fk->city,
            'city' => $fk->city,
            'street' => $fk->streetName,
            'building_no' => "" . rand(13, 500),
            'object_name' => $fk->streetAddress,
            'fake' => 'fake'
        ]);

        $response->assertStatus(201);
        $response->assertJsonStructure(['object_id', 'country','region','city','street',
            'building_no','object_name']);


        $user->refresh();
        $this->assertEquals($count+1, $user->addresses->count());

    }


    public function testCreateFail()
    {
        $fk = Factory::create('ru_RU');

        $user = Client::find(10);
        Sanctum::actingAs($user);

        // Создаем заказ
        $response = $this->post('/v1/addresses', [
            'country' => $fk->realText(500), // Ошибка, слишком большое значение
            'fake' => 'fake'
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure([ 'message', 'errors' => ['country']]);
        $response->assertJsonCount(1, 'errors');

    }


    public function testSuggestions(){

        $user = Client::find(10);
        Sanctum::actingAs($user);

        $response = $this->get('/v1/addresses/suggest?query=Парковая 17 48');
        $response->assertOk();

    }


    public function testDelete(){


        /** @var Address $address */
        $address = Address::withTrashed()->find(19);
        if($address->deleted_at != null){
            $address->restore();
        }

        $oldes = [];
        foreach ($address->orders as $order){
            $oldes[$order->order_id] = $order->order_status_id;
            $order->order_status_id = OrderStatus::ID__INITIAL;
            $order->save();
        }

        Sanctum::actingAs(Client::find(11));
        $response = $this->delete('/v1/addresses/19');
        $response->assertStatus(404);

        Sanctum::actingAs(Client::find(13));
        $response = $this->delete('/v1/addresses/25');
        $response->assertStatus(423);


        foreach ($address->orders as $order){
            $order->order_status_id = OrderStatus::ID__FINISH;
            $order->save();
        }

        Sanctum::actingAs(Client::find(10));
        $response = $this->delete('/v1/addresses/19');
        $response->assertStatus(200);

        $address->refresh();
        $this->assertNotNull($address->deleted_at);


        foreach ($address->orders as $order){
            $order->order_status_id = $oldes[$order->order_id];
            $order->save();
        }
        $address->restore();

    }

}
