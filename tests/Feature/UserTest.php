<?php

namespace Tests\Feature;

use App\Http\Controllers\Api\OrderController;
use App\Models\ContractInvoice;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserTest extends TestCase {



    public function testShowMe() {

        Sanctum::actingAs(Client::find(10));

        $user = Client::find(10);

        $responce = $this->get('/v1/users');
        $responce->assertOk();
        $responce->assertJsonStructure(['name','photo','legal_type','legal_type_name','balance','addresses',
            'invoices','contract_name','contract_url','contacts']);

        if(!empty($user->contract)){
            $this->assertEquals($user->contract->document_number, $responce->json('contract_name'));
        } else {
            $this->assertNull($responce->json('contract_name'));
        }

        $this->assertNotEmpty($responce->json('contacts'));
        $this->assertEquals($user->contact, $responce->json('contacts'));

        $invoices = $responce->json('invoices');
        $this->assertIsArray($invoices);
        $this->assertCount(min(2, $user->invoices()->count()), $invoices);

        $invoice = ContractInvoice::find($invoices[0]['invoice_id']);
        $this->assertNotEmpty($invoice);

        $this->assertEquals($invoice->document_number, $invoices[0]['document_number']);
        $this->assertEquals($invoice->was_paid, $invoices[0]['was_paid']);

        $contract = $responce->json('contract');
        $this->assertArrayHasKey('document_number', $contract);
        $this->assertArrayHasKey('document_date', $contract);
        $this->assertArrayHasKey('contract_id', $contract);
        $this->assertArrayHasKey('name', $contract);
        $this->assertArrayHasKey('download_link', $contract);

        $this->assertNotEmpty($responce->json('photo'));
        $photoResponce = $this->get($responce->json('photo'));
        $photoResponce->assertOk();
        $photoResponce->assertHeader('Content-Type', 'image/jpeg');

    }


    public function testShowAnother() {

        Sanctum::actingAs(Client::find(10));

        $user2 = Client::find(11);
        $responce = $this->get('/v1/users/12');
        $responce->assertOk();
        $responce->assertJsonStructure(['name','photo','legal_type','legal_type_name','balance','addresses',
            'invoices','contract_name','contract_url','contacts']);

        $invoices = $responce->json('invoices');
        $this->assertIsArray($invoices);
        $this->assertCount(1, $invoices);

        $this->assertEquals('3/2021', $invoices[0]['document_number']);
        $this->assertTrue($invoices[0]['was_paid']);

    }

    public function testShowFails(){

        Sanctum::actingAs(Client::find(10));

        $responce = $this->get('/v1/users/123');
        $responce->assertStatus(404);
    }


    public function testUserInvoices(){
        /** @var Client $user */
        Sanctum::actingAs($user = Client::find(13));
        $responce = $this->get('/v1/users/13'); // У него много счетов

        $responce->assertOk();
        $responce->assertJsonStructure(['name','photo','legal_type','legal_type_name','balance','addresses',
            'invoices','contract_name','contract_url','contacts']);

        $invoices = $responce->json('invoices');

        $this->assertIsArray($invoices);
        $this->assertCount(2, $invoices);
        $this->assertGreaterThan($invoices[0]['invoice_id'], $invoices[1]['invoice_id']);
        $this->assertEquals($user->contract->invoices()->max('invoice_id'), $invoices[1]['invoice_id']);


        // Второй пользователь
        Sanctum::actingAs($user = Client::find(12));
        $responce = $this->get('/v1/users/12'); // А у этого только один

        $responce->assertOk();
        $responce->assertJsonStructure(['name','photo','legal_type','legal_type_name','balance','addresses',
            'invoices','contract_name','contract_url','contacts']);

        $invoices = $responce->json('invoices');

        $this->assertIsArray($invoices);
        $this->assertCount(1, $invoices);

    }

}
