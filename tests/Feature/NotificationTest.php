<?php

namespace Tests\Feature;

use App\Http\Controllers\Api\NotificationController;
use App\Models\Notification;
use App\Models\Client;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class NotificationTest extends TestCase
{
    public function testNotifications() {
        Sanctum::actingAs(Client::find(10));

        $count = Notification::count();

        // Список всех уведомлений
        $response = $this->get('/v1/notifications');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['id','title','text']]]);

        $this->assertEquals($count, count($response->json('data')));


        // Первая страница уведомлений
        $response = $this->get('/v1/notifications?page=1');
        $response->assertOk();

        $response->assertJsonStructure([
            'data' => [['id', 'title','text']]]);
        $this->assertEquals(min(NotificationController::IPP, $count), count($response->json('data')));


        $notificationItem = $response->json('data')[0];

        $notification = Notification::find($notificationItem['id']);
        $this->assertNotEmpty($notification);
        // Одно уведомление
        $response = $this->get('/v1/notifications/'.$notificationItem['id']);
        $response->assertOk();
        $response->assertJsonStructure(['id','title','text']);

        $this->assertEquals($notification->title, $response->json('title'));

    }
}
