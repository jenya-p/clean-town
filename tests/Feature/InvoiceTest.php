<?php

namespace Tests\Feature;

use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\OrderController;
use App\Models\ContractInvoice;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class InvoiceTest extends TestCase
{
    public function testInvoiceCreate() {

        $user = Client::find(10);

        $invoiceCount = ContractInvoice::count();

        Sanctum::actingAs($user);

        // Создаем заказ
        $response = $this->post('/v1/invoices', [
            'sum' => 452.25]);

        $response->assertStatus(201);
        $response->assertJsonStructure(['invoice_id','contract_id','document_number','document_date',
                'download_link','total','status_id','name','was_paid']);

        $this->assertDatabaseCount('invoices_to_contract', $invoiceCount + 1);

    }



    public function testInvoiceCreateFail() {

        $user = Client::find(10);

        $invoiceCount = ContractInvoice::count();

        Sanctum::actingAs($user);

        // Создаем заказ
        $response = $this->post('/v1/invoices', [
            'sum' => -452.25]);

        $response->assertStatus(422);
        $response->assertJsonStructure([ 'message', 'errors' => ['sum']]);
        $response->assertJsonCount(1, 'errors');

        $this->assertDatabaseCount('invoices_to_contract', $invoiceCount);

    }



    public function testInvoiceLists(){

        Sanctum::actingAs(Client::find(10));

        // Список всех счетов
        $response = $this->get('/v1/invoices');
        $response->assertOk();

        $response->assertJsonStructure([
            'data' => [['invoice_id', 'name', 'document_number', 'document_date', 'total', 'download_link', 'acts' => []]]
        ]);


        $user = Client::find(10);

        $this->assertEquals($user->contract->invoices()->count(), count($response->json('data')));

        // Первая страница всех счетов
        Sanctum::actingAs(Client::find(13)); // У него много счетов
        $response = $this->get('/v1/invoices?page=1');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['invoice_id', 'name', 'document_number', 'document_date', 'total', 'download_link', 'acts' => []]]
        ]);

        $this->assertEquals(InvoiceController::IPP, count($response->json('data')));

        $maxId = max(array_map(function($itm){return $itm['invoice_id'];}, $response->json('data')));

        // Вторая страница всех счетов
        $response = $this->get('/v1/invoices?page=2');
        $response->assertOk();

        $response->assertJsonStructure([
            'data' => [['invoice_id', 'name', 'document_number', 'document_date', 'total', 'download_link', 'acts' => []]]
        ]);

        $this->assertLessThanOrEqual(OrderController::IPP, count($response->json('data')));

        $minId = min(array_map(function($itm){return $itm['invoice_id'];}, $response->json('data')));

        $this->assertGreaterThan($minId, $maxId);

        // Страница за пределами существующего диапазона. Пустой список
        $response = $this->get('/v1/invoices?page=10000');
        $response->assertOk();
        $this->assertEquals(0, count($response->json('data')));

    }



}
