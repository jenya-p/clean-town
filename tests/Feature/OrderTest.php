<?php

namespace Tests\Feature;

use App\Http\Controllers\Api\OrderController;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\Service;
use App\Models\Staff;
use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class OrderTest extends TestCase {

    public function testOrderCreate() {

        $user = Client::find(10);

        Sanctum::actingAs($user);

        $orderCount = Order::count();
        $photoCount = OrderPhoto::count();

        $name = Str::random(8).'.png';
        $path = resource_path('test_imgs/1.jpg');
        $img = new UploadedFile($path, $name, 'image/jpeg', null, true);

        $addressIds = $user->addresses()->pluck('object_id')->toArray();

        $this->assertNotEmpty($addressIds);

        // Создаем заказ
        $response = $this->post('/v1/orders', [
                'service_id' =>         7,          // Вывоз КГМ и строительных материалов 20м³ 7т IV-V класс опасности
                'type' =>               'Снятие',   //  Снятие
                'count' =>              2,          //  Количество
                'address_id' =>         Arr::random($addressIds),
                'date' =>               Carbon::now()->addDay()->format("Y-m-d"), // String; // YYYY-MM-DD HH:MM
                'time' =>               Order::TIME_FIRSTHALF,
                'loading_required' =>   0,                        // Требуется погрузка
                'comment' =>            'Комментарий 2222',         // Комментарии
                'photo' => $img]);

        $response->assertStatus(201);

        $this->assertDatabaseCount('orders', $orderCount + 1);
        $this->assertDatabaseCount('order_photos', $photoCount + 1);


    }

    public function testOrderCreate2() {

        $user = Client::find(10);

        Sanctum::actingAs($user);

        $orderCount = Order::count();

        $addressIds = $user->addresses()->pluck('object_id')->toArray();

        $this->assertNotEmpty($addressIds);

        // Создаем заказ
        $response = $this->post('/v1/orders', [
            'service_id' =>         7, // Вывоз КГМ и строительных материалов 20м³ 7т IV-V класс опасности
            'type' =>               null,
            'count' =>              2, // Количество
            'address_id' =>         Arr::random($addressIds),
            'date' =>               Carbon::now()->addDay()->format("Y-m-d"), // String; // YYYY-MM-DD HH:MM
            'time' =>               Order::TIME_ANYTIME
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseCount('orders', $orderCount + 1);


    }

    public function testOrderCreateFail1() {

        Sanctum::actingAs(Client::find(10));

        $orderCount = Order::count();
        // Создаем заказ
        $response = $this->post('/v1/orders', [
            'service_id' =>         654, // Ошибка
            'type' =>               15, // Ошибка
            'count' =>              -4, // Ошибка
            'address_id' =>         'asd',// Ошибка
            'date'      =>          '234',// Ошибка
            'time'      =>          'asd',// Ошибка
            'loading_required' =>   'fff', // Ошибка
            'comment' =>            123 // Ошибка

        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message', 'errors' =>
                ['service_id','type','count','address_id', 'date', 'time', 'loading_required','comment']
        ]);
        $response->assertJsonCount(8, 'errors');

        $this->assertDatabaseCount('orders', $orderCount);


    }

    public function testOrderCreateFail2() {

        Sanctum::actingAs(Client::find(10));

        $orderCount = Order::count();

        $response = $this->post('/v1/orders', [
            'service_id' =>  null, // Ошибка
            'type' =>    null,
            'count' =>            null, // Ошибка
            'address_id' =>       null, // Ошибка
            'date' =>        null, // Ошибка
            'time' =>        null, // Ошибка
            'loading_required' => null,
            'comment' =>          null
        ]);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message', 'errors' =>
                ['service_id','count','address_id','date','time']
        ]);
        $response->assertJsonCount(5, 'errors');

        $this->assertDatabaseCount('orders', $orderCount);
    }

    public function testOrderCreateFail3() {

        $user = Client::find(10);

        Sanctum::actingAs($user);

        $orderCount = Order::count();
        $photoCount = OrderPhoto::count();

        $name = Str::random(8).'.png';
        $path = resource_path('test_imgs/1.jpg');
        $img = new UploadedFile($path, $name, 'image/jpeg', null, true);

        $addressIds = $user->addresses()->pluck('object_id')->toArray();

        $this->assertNotEmpty($addressIds);

        // Создаем заказ
        $response = $this->post('/v1/orders', [
            'service_id' =>         5, // Вывоз КГМ и строительных материалов 8м³ 5т IV-V класс опасности
            'type' =>               'Под погрузку', // Недопустимое значение для "Бачок 0,8 м3 (Фаун)"
            'count' =>              2, // Количество
            'address_id' =>         Arr::random($addressIds),
            'date' =>               Carbon::now()->addDay()->format("Y-m-d"),
            'time' =>               Order::TIME_SECONDHALF
        ]);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'message', 'errors' => ['type']
        ]);

        $response->assertJsonCount(1, 'errors');
    }

    public function testOrderShow() {

        Sanctum::actingAs(Client::find(10));

        /** @var Order $order */
        $order = Order::orderByDesc('order_id')->first();
        $responce = $this->get('v1/orders/'.$order->order_id);
        $responce->assertOk();
        // На самом деле полей больше.
        $responce->assertJsonStructure(['id','status','date','address','service_type',
            'service','route','arrive_time', 'contacts','date_from', 'date_to','date', 'time']);

        $this->assertEquals($order->order_id, $responce->json('id'));
        $this->assertEquals($order->status->status_id, $responce->json('status'));
        $this->assertEquals($order->object->object_id, $responce->json('address')['object_id']);

        if(!empty($order->serviceType)){
            $this->assertEquals($order->serviceType->service_type_name, $responce->json('service_type'));
        } else {
            $this->assertNull($responce->json('service_type'));
        }

        $this->assertEquals($order->service->service_name, $responce->json('service')['service_name']);

        $this->assertIsArray($responce->json('route'));
        $this->assertNotEmpty($responce->json('route'));
        $this->assertcount(2, $responce->json('route')[0]);

    }

    public function testOrderContacts(){

        Sanctum::actingAs(Client::find(10));

        $orderId = \DB::selectOne('SELECT order_staff.order_id FROM order_staff GROUP BY order_staff.order_id
	HAVING COUNT(order_staff.staff_id) > 1
	ORDER BY order_id desc
	LIMIT 1');

        if(empty($orderId)){
            $this->addWarning('Не найдено заказов с двумя контактами');
            return;
        }
        $orderId = $orderId->order_id;

        /** @var Order $order */
        $order = Order::find($orderId); // Заказ с двумя контактами
        $responce = $this->get('v1/orders/' . $orderId);

        $responce->assertOk();

        $contacts = $responce->json('contacts');
        $this->assertNotEmpty($contacts);
        foreach ($contacts as $contact){
            $staff = Staff::find($contact['staff_id']);
            $this->assertNotEmpty($staff);
            $this->assertNotEmpty($contact['name']);
            $this->assertEquals($staff->name, $contact['name']);
            $this->assertNotEmpty($staff->phone, $contact['phone']);
            if(!empty($contact['photo_url'])){
                $photoResponce = $this->get($contact['photo_url']);
                $photoResponce->assertOk();
                $photoResponce->assertHeader('Content-Type', 'image/jpeg');
            }

        }

    }

    public function testOrderUpdate() {

        Sanctum::actingAs(Client::find(10));

        $orderCount = Order::count();

        /** @var Order $order */
        $order = Order::orderByDesc('order_id')->first();

        $name = Str::random(8).'.png';
        $path = resource_path('test_imgs/2.jpg');
        $img = new UploadedFile($path, $name, 'image/jpeg', null, true);

        // Обновляем некоторые поля в заказе, не все.
        $response = $this->put('/v1/orders/' . $order->order_id, [
            'service_id' =>         7,
            'type' =>               'Снятие',
            'count' =>              $order->container_amount + 1, // Количество
            'address_id' =>         $order->object_id,
            'date' =>               $order->date->addDay()->format("Y-m-d"), // String; // YYYY-MM-DD HH:MM
            'time' =>               Order::TIME_NIGHT, // String; // YYYY-MM-DD HH:MM
            'loading_required' =>   !$order->loading_required,                        // Требуется погрузка
            'comment' =>            'Комментарий 555',         // Комментарии
            'photo' => $img]);

        $response->assertOk();

        $this->assertDatabaseCount('orders', $orderCount);// Кол-во заказов не увеличивается

        /** @var Order $order2 */
        $order2 = Order::find($order->order_id);

        $this->assertEquals($order->container_amount + 1, $order2->container_amount);

        $this->assertEquals($order->date->addDay(), $order2->date);

        $this->assertNotEquals($order->loading_required, $order2->loading_required);

    }

    public function testOrderLists(){

        Sanctum::actingAs(Client::find(10));

        // Список всех заказов
        $response = $this->get('/v1/orders');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['id', 'status', 'date', 'address' => []]]
        ]);

        $this->assertEquals(Order::where('user_id','=', 10)->count(), count($response->json('data')));

        // Первая страница всех заказов
        $response = $this->get('/v1/orders?page=1');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['id', 'status', 'date', 'address' => []]]
        ]);
        $this->assertEquals(OrderController::IPP, count($response->json('data')));

        $maxId1 = max(array_map(function($itm){return $itm['id'];}, $response->json('data')));

        // Вторая страница всех заказов
        $response = $this->get('/v1/orders?page=2');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['id', 'status', 'date', 'date_from', 'date_to', 'address' => []]]
        ]);
        $this->assertLessThanOrEqual(OrderController::IPP, count($response->json('data')));

        $minId2 = max(array_map(function($itm){return $itm['id'];}, $response->json('data')));

        $this->assertGreaterThan($minId2, $maxId1);

        // Все активные заказы (должен быть хотя бы один)
        $response = $this->get('/v1/orders?status=active');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['id', 'status', 'date', 'date_from', 'date_to', 'address' => []]]
        ]);

        $this->assertLessThan(Order::where('user_id','=', 10)->count(), count($response->json('data')));
        $this->assertGreaterThan(0, count($response->json('data')));

        // Все архивные заказы (должен быть хотя бы один)
        $response = $this->get('/v1/orders?status=archive');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['id', 'status', 'date', 'date_from', 'date_to', 'address' => []]]
        ]);

        $this->assertLessThan(Order::where('user_id','=', 10)->count(), count($response->json('data')));

        // Первая страница архивных заказов
        $response = $this->get('/v1/orders?status=archive&page=1');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['id', 'status', 'date', 'date_from', 'date_to', 'address' => []]]
        ]);

        $this->assertLessThanOrEqual(OrderController::IPP, count($response->json('data')));

        // Страница за пределами существующего диапазона. Пустой список
        $response = $this->get('/v1/orders?status=archive&page=10000');
        $response->assertOk();
        $this->assertEquals(0, count($response->json('data')));

    }

}
