<?php

namespace Tests\Feature;

use App\Models\UserToken;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Http;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class RemoteTest extends TestCase
{

//  use RefreshDatabase;

    const BASE_URL = 'http://ct.studio-205.ru';

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin(){


        $response = Http::post(self::BASE_URL.'/v1/auth', ['login' => 'test@company1.ru', 'password' => '1234567']);
        $this->assertEquals(200, $response->status());
        $plainToken = $response->json('token');
        $this->assertNotEmpty($plainToken);
        $plainToken = preg_replace('/^Bearer/m', '',$plainToken);
        $name = $response->json('name');
        $this->assertEquals('Иван Петров', $name);

        $response = Http::withToken('fake-token')->get(self::BASE_URL.'/v1/orders');
        $this->assertEquals(401, $response->status());

        $response = Http::withToken($plainToken)->get(self::BASE_URL.'/v1/orders?page=1');
        $this->assertEquals(200, $response->status());

    }




}
