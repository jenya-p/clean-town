<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\UserToken;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class LoginTest extends TestCase
{

    public function testBadCredentials(){
        $this->post('/v1/auth',['login' => 'test@company10.ru', 'password' => '123456'])
            ->assertStatus(404);

        $this->post('/v1/auth',['login' => 'test@company.ru', 'password' => '1234567'])
            ->assertStatus(404);

        $this->post('/v1/auth',['login' => '71234556789', 'password' => '1234567'])
            ->assertStatus(404);

        $response = $this->get('/v1/auth');
        $response->assertStatus(405);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin(){

        $response = $this->post('/v1/auth',['login' => 'test@company10.ru', 'password' => '1234567']);
        $response->assertOk();
        $response->assertJsonStructure(['token', 'name']);
        $this->assertEquals('УМ-СКР, ООО', $response->json('name'));
        $this->assertEquals('test@company10.ru', $response->json('email'));
        $plainToken = $response->json('token');
        $this->assertNotEmpty($plainToken);

        $response = $this->post('/v1/auth',['login' => '79121234510', 'password' => '1234567']);
        $response->assertOk();
        $response->assertJsonStructure(['token', 'name']);
        $this->assertEquals('УМ-СКР, ООО', $response->json('name'));
        $this->assertEquals('test@company10.ru', $response->json('email'));
        $plainToken = $response->json('token');
        $this->assertNotEmpty($plainToken);

        $plainToken = preg_replace('/^Bearer/m', '',$plainToken);

        // Тестируем авторизацию через HTTP подключение
        $baseUrl = env('APP_URL');
        $response = Http::withToken('fake-token')->get($baseUrl.'/v1/orders');
        $this->assertEquals(401, $response->status());

        $response = Http::withToken($plainToken)->get($baseUrl.'/v1/orders');
        $this->assertEquals(200, $response->status());

        $response = Http::withToken($plainToken)->get($baseUrl.'/v1/fake-orders-url');
        $this->assertEquals(404, $response->status());

        /** @var UserToken $token */
        $token = UserToken::findToken($plainToken);

        $this->assertNotEmpty($token);

        $this->assertEquals('test@company10.ru', $token->tokenable->email);

        $token->delete();

        $response = Http::withToken($plainToken)->get($baseUrl.'/v1/orders');
        $this->assertEquals(401, $response->status());

    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRefreshTokensFail(){

        $response = $this->post('/v1/auth',['login' => 'test@company10.ru', 'password' => '1234567']);
        $response->assertOk();
        $response->assertJsonStructure(['token', 'name']);
        $this->assertEquals('УМ-СКР, ООО', $response->json('name'));
        $plainToken = $response->json('token');
        $this->assertNotEmpty($plainToken);
        $plainToken = preg_replace('/^Bearer/m', '',$plainToken);

        // Тестируем авторизацию через HTTP подключение
        $baseUrl = env('APP_URL');

        $response = Http::withToken($plainToken)->get($baseUrl.'/v1/orders');
        $this->assertEquals(200, $response->status());

        /** @var UserToken $token */
        $token = UserToken::findToken($plainToken);
        $this->assertFalse($token->isObsolete());

        if(empty(config('sanctum.expiration'))){
            return;
        }

        $newCreationAt = $token->created_at->subMinutes(config('sanctum.expiration') + 20); // Больше чем время жизни сессии
        $token->created_at = $newCreationAt;
        $token->save();
        $this->assertTrue($token->isObsolete());

        $response = Http::withToken($plainToken)->get($baseUrl.'/v1/orders');
        $this->assertEquals(401, $response->status());

        $response = Http::withToken($plainToken)->post($baseUrl.'/v1/refresh_token');
        $this->assertEquals(401, $response->status());

    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRefreshTokensSuccess(){

        $response = $this->post('/v1/auth',['login' => 'test@company10.ru', 'password' => '1234567']);
        $response->assertOk();
        $response->assertJsonStructure(['token', 'name']);
        $this->assertEquals('УМ-СКР, ООО', $response->json('name'));
        $plainToken = $response->json('token');
        $this->assertNotEmpty($plainToken);
        $plainToken = preg_replace('/^Bearer/m', '',$plainToken);

        // Тестируем авторизацию через HTTP подключение
        $baseUrl = env('APP_URL');

        $response = Http::withToken($plainToken)->get($baseUrl.'/v1/orders');
        $this->assertEquals(200, $response->status());

        /** @var UserToken $token */
        $token = UserToken::findToken($plainToken);

        $newCreationAt = $token->created_at->subMinutes(5); // Меньше чем чем время жизни сессии
        $token->created_at = $newCreationAt;
        $token->save();
        $this->assertFalse($token->isObsolete());

        $response = Http::withToken($plainToken)->get($baseUrl.'/v1/orders');
        $this->assertEquals(200, $response->status());

        $response = Http::withToken($plainToken)->post($baseUrl.'/v1/refresh_token');
        $this->assertEquals(200, $response->status());
        $plainToken = $response->json('token');
        $this->assertNotEmpty($plainToken);
        $plainToken = preg_replace('/^Bearer/m', '',$plainToken);

        $response = Http::withToken($plainToken)->get($baseUrl.'/v1/orders');
        $this->assertEquals(200, $response->status());

    }


}
