<?php

namespace Tests\Feature;

use App\Http\Controllers\Api\OrderController;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ServicesTest extends TestCase {

    public function testServices() {

        $user = Client::find(14);
        Sanctum::actingAs($user);

        $response = $this->get('/v1/services');
        $response->assertOk();
        $response->assertJsonStructure(['manager', 'data' => [['id', 'name', 'price', 'service_price', 'available', 'types' => []]]]);

    }

    public function testSupport() {

        $user = Client::find(10);
        Sanctum::actingAs($user);

        $response = $this->get('/v1/support');

        $response->assertOk();
        $response->assertJsonStructure(['data' => [[
            "staff_id","name","email","phone","photo_url","position","status", "telegram", "whatsapp"
        ]]]);


    }

}
