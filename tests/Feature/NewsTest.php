<?php

namespace Tests\Feature\Feature;

use App\Http\Controllers\Api\NewsController;
use App\Models\News;
use App\Models\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class NewsTest extends TestCase {

    public function testNews() {
        Sanctum::actingAs(Client::find(10));

        $count = News::count();

        // Список всех новостей
        $response = $this->get('/v1/news');
        $response->assertOk();
        $response->assertJsonStructure([
            'data' => [['id', 'title','text','media_url']]]);

        $this->assertEquals($count, count($response->json('data')));


        // Первая страница новостей
        $response = $this->get('/v1/news?page=1');
        $response->assertOk();

        $response->assertJsonStructure([
            'data' => [['id', 'title','text','media_url']]]);
        $this->assertEquals(min(NewsController::IPP, $count), count($response->json('data')));


        $newsItem = $response->json('data')[0];

        $news = News::find($newsItem['id']);
        $this->assertNotEmpty($news);
        // Одна новость
        $response = $this->get('/v1/news/'.$newsItem['id']);
        $response->assertOk();
        $response->assertJsonStructure(['id','title','text','media_url','news_url']);

        $this->assertEquals($news->title, $response->json('title'));

    }
}
