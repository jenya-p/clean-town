<?php

namespace Tests\Feature;

use App\Http\Controllers\Api\InvoiceController;
use App\Http\Controllers\Api\OrderController;
use App\Models\ContractInvoice;
use App\Models\Order;
use App\Models\OrderPhoto;
use App\Models\ReconcileReport;
use App\Models\Client;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ReconcileReportTest extends TestCase
{
    public function testCreate() {

        $user = Client::find(10);
        Sanctum::actingAs($user);

        $reportCount = ReconcileReport::count();

        // Создаем заказ
        $response = $this->post('/v1/reconcile_reports', [
            'sum' => 452.25]);

        $response->assertStatus(201);
        $response->assertJsonStructure(['report_id','user_id','document_number','document_date',
                'download_link','name']);

        $this->assertDatabaseCount('reconcile_reports', $reportCount + 1);

    }

}
