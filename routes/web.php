<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/order-photo/{order_photo}', 'App\Http\Controllers\Controller@orderPhoto')
    ->name('photo.order');

Route::get('/user-photo/{user}', 'App\Http\Controllers\Controller@userPhoto')
    ->name('photo.user');

Route::get('/user-staff/{staff}', 'App\Http\Controllers\Controller@staffPhoto')
    ->name('photo.staff');

Route::get('/news-media/{news}', 'App\Http\Controllers\Controller@newsMedia')
    ->name('news-media');


// TODO Это зашлушка для тестирования. Потом удалить!
Route::get('/test-image.jpg', function(){
    return response()->file(resource_path('test_imgs/1.jpg'));
})->name('photo.test');
