<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('auth', 'UserController@login');

Route::middleware('auth:sanctum')->group(function(){

    Route::post('refresh_token', 'UserController@refreshToken');

    Route::get('users/{user?}', 'UserController@show');

    Route::apiResource('orders', 'OrderController');

    Route::apiResource('news', 'NewsController')->only('index', 'show');

    Route::apiResource('notifications', 'NotificationController')->only('index', 'show');

    Route::apiResource('invoices', 'InvoiceController')->only('store', 'index');

    Route::get('addresses/suggest', 'AddressesController@suggest');
    Route::apiResource('addresses', 'AddressesController')->only('store', 'destroy');

    Route::get('services', 'ServicesController@index');

    Route::get('support', 'SupportController@index');

    Route::apiResource('reconcile_reports', 'ReconcileReportController')->only('store');

});
